import java.util.*;

/**
 * Solution to Problem C: "Enemy of my Enemy is my Friend" from the 2016 
 * ACM North Central Regional Programming Contest.
 *
 * @author Mark M. Meysenburg
 * @version 10/30/2016
 */
public class ProbC {

    /**
     * Create a map of person names to array indices. First person 
     * mentioned maps to array index 0, and so on.
     *
     * @param lines List of lines containing line type, number of names, 
     * and names.
     * @return HashMap that maps names to array indices.
     */
    private static HashMap<String, Integer> mapIndices(
            List<String> lines) {
        HashMap<String, Integer> map = new HashMap<>();

        int idx = 0;
        for (String line : lines) {
            Scanner scn = new Scanner(line);
            // don't care about line type here
            String t = scn.next();

            int n = scn.nextInt();
            for (int i = 0; i < n; i++) {
                String name = scn.next();
                if (!map.containsKey(name)) {
                    map.put(name, idx);
                    idx++;
                } // if name isn't in map
            } // for names
        } // for lines

        return map;
    }

    /**
     * Build the 2d of initial relationships between people. A cell in 
     * the array contains -1 (enemy), 0 (neutral), or 1 (friend).
     *
     * @param idxMap Map mapping names to indices
     * @param lines List of lines containing line type, number of names, 
     * and names.
     * @return 2d array showing the initial (non-deduced) relationships 
     * between people.
     */
    private static int[][] initRelations(Map<String, Integer> idxMap,
            List<String> lines) {
        int[][] rels = new int[idxMap.size()][idxMap.size()];

        for (String line : lines) {
            Scanner scn = new Scanner(line);
            String type = scn.next();
            LinkedList<String> names = new LinkedList<>();

            int n = scn.nextInt();
            for (int i = 0; i < n; i++) {
                String name = scn.next();
                names.add(name);
            } // for names

            int val = type.equals("e") ? -1 : 1;
//            int val = 0;
//            if(type.equals("e")) {
//                val = -1;
//            } else {
//                val = 1;
//            }

            // look at all the pairs of names in the line
            for (int i = 0; i < names.size() - 1; i++) {
                for (int j = i + 1; j < names.size(); j++) {
                    int o = idxMap.get(names.get(i));
                    int p = idxMap.get(names.get(j));
                    rels[o][p] = val;
                    rels[p][o] = val;
                }
            }

        } // for lines

        return rels;
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int lines = stdin.nextInt();
        int lists = stdin.nextInt();

        int caseNum = 1;

        // eat the end of the first line
        stdin.nextLine();

        while (!(lines == 0 && lists == 0)) {

            System.out.printf("Case %d: ", caseNum);

            LinkedList<String> lineList = new LinkedList<>();

            for (int i = 0; i < lines; i++) {
                String s = stdin.nextLine();
                lineList.add(s);
            } // for lines

            // map names to indices of a 2d array
            HashMap<String, Integer> idxMap = mapIndices(lineList);

            // create initial array of relationships
            int[][] rels = initRelations(idxMap, lineList);

            System.out.println();
            System.out.println(idxMap);
            System.out.println();
            for (int i = 0; i < rels.length; i++) {
                for (int j = 0; j < rels[i].length; j++) {
                    System.out.printf("%+d ", rels[i][j]);
                }
                System.out.println();
            }
//            System.exit(0);

            // TODO: ENEMY OF MY ENEMY IS MY FRIEND, i.e., the whole point
            
            for (int i = 0; i < lists; i++) {
                int num = stdin.nextInt();
                LinkedList<String> names = new LinkedList<>();
                for (int j = 0; j < num; j++) {
                    
                    String name = stdin.next();
                    names.add(name);
                } // for names in the list  
//                System.err.println(names);
                boolean saidNo = false;
                for(int j = 0; j < names.size() - 1; j++) {
                    String n1 = names.get(j);
                    for(int k = j + 1; k < names.size(); k++) {
                        String n2 = names.get(k);
//                        System.err.println(n1 + ", " + n2);
                        int i1 = idxMap.get(n1);
                        int i2 = idxMap.get(n2);
                        
                        if(rels[i1][i2] == -1) {
                            System.out.print("no ");
                            k = names.size();
                            j = k;
                            saidNo = true;
                        }
                    }
                }
                if(!saidNo) {
                    System.out.print("yes ");
                }
            }
            System.out.println();
            caseNum++;
            lines = stdin.nextInt();
            lists = stdin.nextInt();
            stdin.nextLine();
        } // while 
    } // main 
} // ProbC
