
import java.util.*;

public class ProbE {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int x = stdin.nextInt();
        int y = stdin.nextInt();
        int n = stdin.nextInt();

        int caseNum = 1;

        while (!(x == 0 && y == 0 && n == 0)) {
            System.out.printf("Case %d:\n", caseNum);

            for (int i = 1; i <= n; i++) {
                boolean printed = false;
                if (i % x == 0) {
                    System.out.print("Fizz");
                    printed = true;
                }
                if (i % y == 0) {
                    System.out.print("Buzz");
                    printed = true;
                }
                if (!printed) {
                    System.out.println(i);
                } else {
                    System.out.println("");
                }
            }

            caseNum++;

            x = stdin.nextInt();
            y = stdin.nextInt();
            n = stdin.nextInt();
        } // while
    } // main
} // ProbE
