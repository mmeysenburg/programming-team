import java.util.*;

public class ProbA {
    public static void main(String[] args) {
	Scanner stdin = new Scanner(System.in);

	int x1 = stdin.nextInt();
	int x2 = stdin.nextInt();

	int caseNum = 1;
	while(!(x1 == 0 && x2 == 0) ) {
	    int n1 = stdin.nextInt();
	    ArrayList<Integer> t1 = new ArrayList<>();
	    for(int i = 0; i < n1; i++) {
		t1.add(stdin.nextInt());
	    }

	    int n2 = stdin.nextInt();
	    ArrayList<Integer> t2 = new ArrayList<>();
	    for(int i = 0; i < n2; i++) {
		t2.add(stdin.nextInt());
	    }

	    // swap if needed
	    if(x1 > x2) {
		int t = x1;
		x1 = x2;
		x2 = t;

		ArrayList<Integer> temp = t1;
		t1 = t2;
		t2 = temp;
	    }

	    boolean x1m = false;
	    boolean x2m = false;
	    boolean done = false;
	    int time = 1;
	    String s = "safe and sound";
	    while(!done) {
		if((x1 + 4.4) >= x2) {
		    done = true;
		    s = "bumper tap at time " + time;
		}
		
		if(t1.contains(time)) {
		    x1m = !x1m;
		}
		if(t2.contains(time)) {
		    x2m = !x2m;
		}

		
		
		time++;
	    }
	    
	    System.out.printf("Case %d: %s\n", caseNum, s);

	    caseNum++;

	    x1 = stdin.nextInt();
	    x2 = stdin.nextInt();
	}
    } // main
    
} // ProbA
