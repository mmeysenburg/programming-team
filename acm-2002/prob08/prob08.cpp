// solution to problem 8, 2002 ACM programming contest
//
// Mark M. Meysenburg, 11/08/2002

#include <iostream>
#include <cmath>
using namespace std;

// function to execute the dfa
int runDFA(int dfa[2][101], int inputString[], int length);

int main()
{
	int N, S, F, i, j, k, e0, e1;
	int dfa[2][101];
	int finals[11];
	int caseNum = 1;
	bool zeroFlag = false;
	int inputString[1025];
	int numStrings = 0;
	int endState;
	int acceptedCount;

	cin >> N >> S >> F;

	while(N != 0 && S != 0 && F != 0)
	{
		// blot out previous dfa
		for(i = 0; i < 2; i++)
			for(j = 0; j < 101; j++)
				dfa[i][j] = 0;

		for(i = 0; i < 11; i++)
			finals[i] = 0;

		zeroFlag = false;

		for(int i = 0; i < 1025; i++)
			inputString[i] = 0;

		// get the edges
		for(i = 1; i <= S; i++)
		{
			cin >> e0 >> e1;
			dfa[0][i] = e0;
			dfa[1][i] = e1;
		}

		// get the finals
		for(i = 1; i <= F; i++)
		{
			cin >> finals[i];
			if(finals[i] == 1)
				zeroFlag = true;
		}

		// output the easy case: strings of length 0
		cout << "Case " << caseNum++ << ":" << endl;
		cout << "    Length = 0, ";
		if(zeroFlag)
			cout << 1 << " string accepted." << endl;
		else
			cout << 0 << " strings accepted." << endl;

		// now start running the strings
		for(i = 1; i <= N; i++)
		{
			acceptedCount = 0;

			numStrings = (int)pow(2, i);
			for(j = 0; j < numStrings; j++)
			{
				endState = runDFA(dfa, inputString, i);

				for(k = 1; k <= F; k++)
					if(finals[k] == endState)
					{
						acceptedCount++;
						break;
					}

				// update the string to the next int
				if(inputString[0] == 0)
					inputString[0] = 1;
				else
				{
					inputString[0] = 0;
					k = 1;
					while(inputString[k] == 1)
					{
						inputString[k] = 0;
						k++;
					}
					inputString[k] = 1;
				}

			} // for j = 0 to numStrings - 1

			// erase the input string to prepare for the next
			// length
			for(j = 0; j < 1025; j++)
				inputString[j] = 0;

			cout << "    Length = " << i << ", ";
			cout << acceptedCount;
			if(acceptedCount == 1)
				cout << " string accepted." << endl;
			else
				cout << " strings accepted." << endl;

		} // for i = 1 to N

		cin >> N >> S >> F;
	}

	return 0;
}

int runDFA(int dfa[2][101], int inputString[], int length)
{
	int i, state = 1;

	for(i = 0; i < length; i++)
		state = dfa[inputString[i]][state];

	return state;
}