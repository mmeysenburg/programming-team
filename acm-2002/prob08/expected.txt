Case 1:
    Length = 0, 0 strings accepted.
    Length = 1, 0 strings accepted.
    Length = 2, 0 strings accepted.
    Length = 3, 0 strings accepted.
Case 2:
    Length = 0, 0 strings accepted.
    Length = 1, 1 string accepted.
    Length = 2, 2 strings accepted.
    Length = 3, 4 strings accepted.
Case 3:
    Length = 0, 0 strings accepted.
    Length = 1, 0 strings accepted.
    Length = 2, 0 strings accepted.
    Length = 3, 0 strings accepted.
    Length = 4, 0 strings accepted.
    Length = 5, 32 strings accepted.
    Length = 6, 0 strings accepted.
    Length = 7, 0 strings accepted.
    Length = 8, 0 strings accepted.
    Length = 9, 0 strings accepted.
    Length = 10, 0 strings accepted.
Case 4:
    Length = 0, 0 strings accepted.
    Length = 1, 0 strings accepted.
    Length = 2, 0 strings accepted.
    Length = 3, 0 strings accepted.
    Length = 4, 0 strings accepted.
    Length = 5, 0 strings accepted.
    Length = 6, 0 strings accepted.
    Length = 7, 0 strings accepted.
    Length = 8, 0 strings accepted.
    Length = 9, 1 string accepted.
    Length = 10, 2 strings accepted.
Case 5:
    Length = 0, 1 string accepted.
    Length = 1, 2 strings accepted.
    Length = 2, 4 strings accepted.
    Length = 3, 8 strings accepted.
    Length = 4, 16 strings accepted.
    Length = 5, 32 strings accepted.
    Length = 6, 64 strings accepted.
    Length = 7, 128 strings accepted.
    Length = 8, 256 strings accepted.
    Length = 9, 512 strings accepted.
    Length = 10, 1024 strings accepted.
Case 6:
    Length = 0, 0 strings accepted.
    Length = 1, 0 strings accepted.
    Length = 2, 0 strings accepted.
    Length = 3, 2 strings accepted.
Case 7:
    Length = 0, 0 strings accepted.
    Length = 1, 0 strings accepted.
    Length = 2, 0 strings accepted.
    Length = 3, 0 strings accepted.
    Length = 4, 1 string accepted.
    Length = 5, 0 strings accepted.
    Length = 6, 0 strings accepted.
    Length = 7, 0 strings accepted.
    Length = 8, 0 strings accepted.
    Length = 9, 0 strings accepted.
    Length = 10, 0 strings accepted.

