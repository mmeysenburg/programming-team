import java.math.*;
import java.util.*;

/**
 * Solution for problem 7 of the 2002 ACM programming contest.
 *
 * @author Mark M. Meysenburg'
 * @version 11/11/2004
 */
public class prob07
{
    public static void main(String[] args)
    {
	Scanner stdin = new Scanner(System.in);

	int Case = 1;

	while(stdin.hasNextLine()) {
	    String line = stdin.nextLine();
	    
	    // set up a StringTokenizer to use whitespace, plus + and -,
	    // as tokens
	    StringTokenizer tzr = new StringTokenizer(line, "+-=", true);

	    if(tzr.hasMoreTokens()) {
		// get the tokens from the line
		String n1 = tzr.nextToken().trim();
		String op = tzr.nextToken().trim();
		String n2 = tzr.nextToken().trim();
		String eq = tzr.nextToken().trim();
		String n3 = tzr.nextToken().trim();

		int smallestBase = -1;

		// now, try to force each string representation into a 
		// BigInteger, using each base 2..36; if the numbers
		// work for the base, add 'em and see if they equal the
		// result. If they do, we've found the smallest base
		// that these values make sense for
		for(int base = 2; base <= 36; base++) {
		    
		    // use try / catch to skip over instances where the
		    // numbers don't make sense in the specified base
		    try {
			BigInteger bi1 = new BigInteger(n1, base);
			BigInteger bi2 = new BigInteger(n2, base);
			BigInteger bi3 = new BigInteger(n3, base);
			
			// see if the numbers add up to the result
			BigInteger result = null;
			if(op.equals("+"))
			    result = bi1.add(bi2);
			else
			    result = bi1.subtract(bi2);

			if(result.equals(bi3)) {
			    smallestBase = base;
			    break;
			}
		    }
		    // empty catch; don't do anything at all if 
		    // the bases don't work
		    catch(NumberFormatException nfe) { }

		} // for each base

		// output results
		if(smallestBase == -1)
		    System.out.println("Case " + Case++ +
				       ": expression is invalid.");
		else
		    System.out.println("Case " + Case++ +
				       ": minimum base is " + 
				       smallestBase);

	    } // if there are tokens

	} // while there are lines

    } // main

} // public class prob07

/**
 * Proof of concept for how to solve the "Off Base" problem using Java.
 * The hard part would be reading in the gunk from the keyboard. :)
 */
// public class prob07
// {
//     public static void main(String[] args)
//     {
// 	String n1 = new String("2N"), 
// 	    n2 = new String("M"), 
// 	    n3 = new String("3I");

// 	int minBase = -1;

// 	for(int i = 36; i >= 2; i--) {
// 	    try {
// 		BigInteger N1 = new BigInteger(n1, i);
// 		BigInteger N2 = new BigInteger(n2, i);
// 		BigInteger N3 = new BigInteger(n3, i);
// 		BigInteger result = N1.add(N2);

// 		if(N3.compareTo(result) == 0)
// 		    minBase = i;
// 	    }
// 	    catch(NumberFormatException e) {
// 	    }

// 	} // for

// 	if(minBase == -1)
// 	    System.out.println("expression is invalid.");
// 	else
// 	    System.out.println("minimum base is " + minBase);

//     } // main

// } // public class prob07
