#include <iostream>
#include <fstream>
using namespace std;

int tick;

class Block
{
public:
	Block(int low = -1, int high = -1, int last = 0) : 
	  lowAddress(low), highAddress(high), lastTick(last) { }
	  int lowAddress;
	  int highAddress;
	  int lastTick;

	  friend ostream& operator<< (ostream& ostr, const Block& block);
};

ostream& operator<< (ostream& ostr, const Block& block)
{
	ostr << "[" << block.lowAddress << ", " << 
		block.highAddress << ", " << 
		block.lastTick << "]";

	return ostr;
}

class Cache
{
public:
	Cache(int bs = 0, int ts = 0, int at = 0);
	~Cache();
	int load(int address);
	void update(int la, int ha, int address);
	int blockSize;
	int totalSize;
	int accessTime;
	Block* blocks;
	int numBlocks;
	Cache* prevCache;
	Cache* nextCache;
	int mainMemoryTime;

	friend ostream& operator<< (ostream& ostr, const Cache& cache);
};

ostream& operator<< (ostream& ostr, const Cache& cache)
{
	for(int i = 0; i < cache.numBlocks; i++) 
		ostr << "Block: " << i << ": " << cache.blocks[i] << endl;

	return ostr;
}

Cache::Cache(int bs, int ts, int at) :
blockSize(bs), totalSize(ts), accessTime(at), blocks(0), numBlocks(0),
prevCache(0), nextCache(0), mainMemoryTime(0)
{
	if(blockSize != 0) {
		numBlocks = totalSize / blockSize;
		blocks = new Block[numBlocks];

		//     for(int i = 0; i < numBlocks; i++) 
		//       cerr << (i + 1) << ": " << blocks[i] << endl;
	}
}

Cache::~Cache()
{
	if(blocks != 0)
		delete [] blocks;
}

int Cache::load(int address)
{
	// is the address in one of our blocks?
	bool found = false;
	for(int i = 0; i < numBlocks; i++)
		if(address >= blocks[i].lowAddress && address <= blocks[i].highAddress) {
			found = true;
			break;
		}

		if(found) {
			// two things to do: 1) update the higher level cache, and
			// 2) return our access time
			if(prevCache != 0) {
				int bs = prevCache->blockSize;
				// find addresses of block to update in the higher cache
				int la = (address / bs) * bs;
				int ha = la + bs - 1;
				prevCache->update(la, ha, address);
			} // updating higher level cache

			return accessTime;

		} // found in this level

		else {

			if(nextCache != 0)
				return accessTime + nextCache->load(address);
			else {
				// load from main memory into this cache
				update((address / blockSize) * blockSize, 
					(address / blockSize) * blockSize + blockSize - 1,
					address);
				//// if we're going to main memory, then we need to start
				//// of the cache updating process
				//if(prevCache != 0) {
				//	int bs = prevCache->blockSize;
				//	int la = address / bs;
				//	int ha = la + bs;
				//	prevCache->update(la, ha, address);
				//}

				return accessTime + mainMemoryTime;
			}

		} // not found in this level
}

void Cache::update(int la, int ha, int address)
{
	// see if there's an open block; if so, put the addresses
	// in it
	bool found = false;
	int blockIndex = -1;

	for(int i = 0; i < numBlocks; i++)
		if(blocks[i].lowAddress == -1) {
			found = true;
			blockIndex = i;
			break;
		}

		if(found) {
			blocks[blockIndex].lowAddress = la;
			blocks[blockIndex].highAddress = ha;
			blocks[blockIndex].lastTick = ++tick;

		} // empty block found

		else {

			// search for least recently used block and
			// replace it
			int smallestTick = INT_MAX;
			for(int i = 0; i < numBlocks; i++)
				if(blocks[i].lastTick < smallestTick) {
					smallestTick = blocks[i].lastTick;
					blockIndex = i;
				}

				blocks[blockIndex].lowAddress = la;
				blocks[blockIndex].highAddress = ha;
				blocks[blockIndex].lastTick = ++tick;

		} // no empty block found

		// now we need to follow the chain upwards, updating as need be
		if(prevCache != 0) {

			int bs = prevCache->blockSize;
			int la = (address / bs) * bs;
			int ha = la + bs - 1;

			prevCache->update(la, ha, address);

		} // if there is a previous cache

}

int main()
{
	ifstream in("input5.txt");
	ofstream out("output.txt");

	int numCase = 1, numLevels;

	//	cin >> numLevels;
	in >> numLevels;

	while(numLevels != 0) {

		tick = 0;

		Cache** caches = new Cache*[numLevels];

		// read info for each level
		for(int level = 0; level < numLevels; level++) {

			int blockSize, totalSize, accessTime;
			//			cin >> blockSize >> totalSize >> accessTime;
			in >> blockSize >> totalSize >> accessTime;

			caches[level] = new Cache(blockSize, totalSize, accessTime);

		}

		// hook up the links between cache levels
		for(int level = 0; level < numLevels - 1; level++)
			caches[level]->nextCache = caches[level + 1];
		for(int level = 1; level < numLevels; level++)
			caches[level]->prevCache = caches[level - 1];

		int mainMemoryAccessTime;
		//		cin >> mainMemoryAccessTime;
		in >> mainMemoryAccessTime;

		// set main memory access time field of the last cache
		caches[numLevels - 1]->mainMemoryTime = mainMemoryAccessTime;

		int numAddresses;
		//		cin >> numAddresses;
		in >> numAddresses;

		int totalTime = 0;

		for(int i = 0; i < numAddresses; i++) {
			int address;
			//			cin >> address;
			in >> address;
			totalTime += caches[0]->load(address);

			out << "Accessing address " << address << endl;
			for(int j = 0; j < numLevels; j++)
				out << *(caches[i]) << endl;
			out << "\n";
		}

		//cout << "Case " << numCase++ << ": total time = " <<
		//	totalTime << " nanoseconds" << endl;
		out << "Case " << numCase++ << ": total time = " <<
			totalTime << " nanoseconds" << endl;

		for(int i = 0; i < numLevels; i++)
			delete caches[i];
		delete [] caches;

		//cin >> numLevels;
		in >> numLevels;

	} // while there are more cases

	return 0;
}
