// Solution to problem one from the 2002 ACM programming contest, 
// 11/09/2002.
//
// Mark M. Meysenburg

#include <iostream>
#include <cmath>
using namespace std;

// given N items, and K answers per question, return M, the number
// of questions required to guess the item.
unsigned long howManyQuestions(double K, double N)
{
	return (unsigned long)(ceil(log(N) / log(K)) + 1);
}

// main is just input processing loop
int main()
{
	double K = -1, N = -1;

	cin >> K >> N;

	while(K != 0 && N != 0)
	{
		cout << (unsigned long)N << " items, " 
			<< (unsigned long)K << " answers per question, "
			<< howManyQuestions(K, N) << " questions"
			<< endl;
		cin >> K >> N;
	}

	return 0;
}
