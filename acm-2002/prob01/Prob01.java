import java.util.*;

/**
 * Solution to problem one from the 2002 ACM programming contest,
 * 11/09/2002.
 *
 * @author Mark M. Meysenburg
 * @version 11/10/2004
 */
public class Prob01
{
    /**
     * Calculate the number of questions required to guess the item.
     *
     * @param K Number of answers per question.
     * @param N Number of items.
     * @return M, the number of questions required.
     */
    private static long howManyQuestions(double K, double N)
    {
        return (long)(Math.ceil(Math.log10(N) / Math.log10(K)) + 1);
    }
    
    /**
     * Application entry point; just an input processing loop.
     *
     * @param args Command line arguments; ignored by this program.
     */
    public static void main(String[] args)
    {
        Scanner stdin = new Scanner(System.in);
        
        double K = -1, N = -1;
        
        K = stdin.nextDouble();
        N = stdin.nextDouble();
        
        while(!(K == 0 && N == 0))
        {
            System.out.println((long)N + " items, " + 
                (long)K + " answers per question, " +
                howManyQuestions(K, N) + " questions");
            
            K = stdin.nextDouble();
            N = stdin.nextDouble();
        }
    }
}
