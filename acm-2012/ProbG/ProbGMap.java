
import java.util.TreeMap;
import java.util.Map;
import java.util.Scanner;

public class ProbGMap {
    
    private static Map<Integer, Integer> map;
    
    private static long mapHits = 0l;
    
    private static int extFib(int n, int k, int q) {

        if(map.containsKey(n)) {
            mapHits = mapHits + 1l;
            return map.get(n);
        }
        
        if((n >= 0) && (n < k)) {
            map.put(n, 1);
            return 1;
        } else {
            int sum = 0;
            
            for(int i = 1; i <= k; i++) {
                sum += (extFib(n - i, k, q) % q);
            }
            
            sum = sum % q;
            
            map.put(n, sum);
            return sum;
        }
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        
        int n = stdin.nextInt();
        int k = stdin.nextInt();
        int q = stdin.nextInt();
        int caseNum = 1;
        
        while(!(n == 0 && k == 0 && q == 0)) {
            
            map = new TreeMap<>();
            System.out.printf("Case %d: %d\n", caseNum++, extFib(n, k, q));
            
            n = stdin.nextInt();
            k = stdin.nextInt();
            q = stdin.nextInt();
        }
        
        System.err.println("Map hits: " + mapHits);
    }
}
