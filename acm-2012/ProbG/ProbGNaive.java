
import java.util.Scanner;

public class ProbGNaive {
    
    private static int extFib(int n, int k, int q, int d) {
        if((n >= 0) && (n < k)) {
            return 1;
        } else {
            long sum = 0;
            
            for(int i = 1; i <= k; i++) {
                sum += extFib(n - i, k, q, d + 1) % q;
            }
            
            return (int)(sum % q);
        }
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        
        int n = stdin.nextInt();
        int k = stdin.nextInt();
        int q = stdin.nextInt();
        int caseNum = 1;
        
        while(!(n == 0 && k == 0 && q == 0)) {
            
            try {
                System.out.printf("Case %d: %d\n", caseNum++, extFib(n, k, q, 0));
            } catch(StackOverflowError sfe) {
                System.err.println("\tStack overflow! Skipping");
            }
            
            n = stdin.nextInt();
            k = stdin.nextInt();
            q = stdin.nextInt();
        }
    }
}
