
import java.util.Scanner;

public class RecFib {
    public static long fib(long n) {
        if(n <= 1L) {
            return 1L;
        } else {
            return fib(n - 1L) + fib(n - 2L);
        }    
    }
    
    public static void main(String[] args) {
        for(int i = 1; i <= 100; i++) {
            System.out.println("Fib(" + i + ") = " + fib(i));
        }
    }
}