
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ModTest {
    public static void main(String[] args) {
        Random prng = new Random();
        for(int trial = 0; trial < 1000000; trial++) {
            System.out.print("Trial: " + trial + "...");
            List<Integer> numbers = new LinkedList<>();
            int q = prng.nextInt(3000) + 1;
            for(int i = 0; i < 10000; i++) {
                numbers.add(prng.nextInt(3000) + 1);
            }
            
            // sum then mod
            int sum = 0;
            for(int i : numbers) {
                sum += i;
            }
            int methodA = sum % q;
            
            // sum of mods, then mod
            sum = 0;
            for(int i : numbers) {
                sum += (i % q);
            }
            int methodB = sum % q;
            
            if(methodA != methodB) {
                System.out.println("FAILED!!!");
                System.exit(-1);
            } else {
                System.out.println("passed");
            }
        }
    }
}