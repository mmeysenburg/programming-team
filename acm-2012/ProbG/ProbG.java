import java.util.Scanner;

/**
 * Solution to Problem G: Fibonacci Extended from the 2012 ACM Programming
 * Contest. 
 * 
 * @author Mark M. Meysenburg
 * @version 04/27/2017
 */
public class ProbG {
    
    /**
     * Iteratively compute the extended Fibonacci function. 
     * 
     * @param n Integer in (0, 1000000)
     * @param k Integer in (0, 1000000)
     * @param q Integer in (1, 1073741824)
     * @return F(n, k, q)
     */
    private static int extFib(int n, int k, int q) {
        // if we have the simple case, just return 1 right away
        if(n < k) {
            return 1;
        } else {
            // otherwise, make the array holding our sequence
            int[] f = new int[n + 1];
            
            // populate the first k elements (spots 0 thorugh k - 1)
            // the the value 1
            for(int i = 0; i < k; i++) {
                f[i] = 1;
            }
            
            // and for the rest of the way up to n,
            for(int i = k; i <= n; i++) {
                
                // compute F(i, k, q) via iteration
                for(int j = 1; j <= k; j++) {
                    f[i] += (f[i - j] % q);
                }
                f[i] = f[i] % q;
            }
            
            // once we're done with the loops, our answer is in 
            // f[n]
            return f[n];
        }
    }
    
    /**
     * Application entry point. 
     * 
     * @param args Command-line arguments; ignored by this application. 
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        
        int n, k, q;
        
        n = stdin.nextInt();
        k = stdin.nextInt();
        q = stdin.nextInt();
        
        int caseNum = 1;
        while(!(n == 0 && k == 0 && q == 0)) {
            int ans = extFib(n, k, q);
            
            System.out.printf("Case %d: %d\n", caseNum, ans);
            caseNum++;
            
            n = stdin.nextInt();
            k = stdin.nextInt();
            q = stdin.nextInt();
        }
    }
}