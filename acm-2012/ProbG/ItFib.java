public class ItFib {
    public static long fib(int n) {
        if(n <= 1L) {
            return 1L;
        } else {
            long sum = 0L;
            long m2 = 1L;
            long m1 = 1L;
            for(int i = 2; i <= n; i++) {
                sum = (m1 + m2);
                m2 = m1;
                m1 = sum;
            }
            
            return sum;
        }    
    }
    
    public static void main(String[] args) {
        for(int i = 1; i <= 100; i++) {
            System.out.println("Fib(" + i + ") = " + fib(i));
        }
    }
}