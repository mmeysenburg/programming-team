import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mark.meysenburg
 */
public class ProbG {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int i = 1, n;
        n = stdin.nextInt();
        while (n > 0) {
            int[] x = new int[n];
            int[] y = new int[n];
            int[] m = new int[n];

            for (int j = 0; j < n; j++) {
                x[j] = stdin.nextInt();
                y[j] = stdin.nextInt();
                m[j] = stdin.nextInt();
            }

            double a = 0.0, b = 0.0;
            double M_x, M_y, last;

            // compute M_x
            M_x = Double.POSITIVE_INFINITY;
            while (Math.abs(M_x) > 0.004) {

                M_x = 0.0;

                for (int j = 0; j < n; j++) {
                    M_x += m[j] * (b - y[j]);
                }
                if (Math.abs(M_x) > 0.004) {
                    if (M_x > 0) {
                        b -= 0.0001;
                    } else {
                        b += 0.0001;
                    }
                }
            }

            // compute M_y
            M_y = Double.POSITIVE_INFINITY;
            while (Math.abs(M_y) > 0.004) {
                
                M_y = 0.0;
                        
                for (int j = 0; j < n; j++) {
                    M_y += m[j] * (a - x[j]);
                }
                if(Math.abs(M_y) > 0.004) {
                    if(M_y > 0) {
                        a -= 0.0001;
                    } else {
                        a += 0.0001;
                    }
                }
            }

            System.out.printf("Case %d: %.2f %.2f\n", i, a, b);
            i++;

            n = stdin.nextInt();
        } // while
    } // main
} // ProbG
