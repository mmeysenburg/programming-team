
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mark.meysenburg
 */
public class ProbE {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        
        int i = 1;
        while(stdin.hasNextLong()) {
            long N = stdin.nextLong();
            long t = stdin.nextLong();
            long b = stdin.nextLong();
            
            if(t > N) {
                t = t % N;
            }
            
            boolean on = false;
            for(long j = 1; j <= t; j++) {
                if(b % j == 0) {
                    on = !on;
                }
            }
            
            String s = on ? "On" : "Off";
            System.out.printf("Case %d: %s\n", i, s);
            i++;
        } // while
    } // main
} // ProbE
