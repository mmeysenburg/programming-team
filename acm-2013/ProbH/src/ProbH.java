
import java.util.Arrays;
import java.util.Scanner;

public class ProbH {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String a, b, c;
        a = stdin.nextLine();
        b = stdin.nextLine();
        int i = 1;
        while(!(a.equals("END") && b.equals("END"))) {
            
            // strategy: convert each string to a character array, sort 'em,
            // convert back to strings, and compare for equality
            char[] aChars = a.toCharArray();
            char[] bChars = b.toCharArray();
            
            Arrays.sort(aChars);
            Arrays.sort(bChars);
            
            a = new String(aChars);
            b = new String(bChars);
            
            c = (a.equals(b)) ? "same" : "different";
            
            System.out.printf("Case %d: %s\n", i, c);
            
            i++;
            
            a = stdin.nextLine();
            b = stdin.nextLine();
        } // while
    } // main
} // ProbH
