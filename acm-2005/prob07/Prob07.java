import java.util.*;

public class Prob07
{
    public void run() 
    {
	Scanner stdin = new Scanner(System.in);

	int numCases = stdin.nextInt();

	for(int caseNo = 1; caseNo <= numCases; caseNo++) {

	    LinkedList<Book> shelf = new LinkedList<Book>();

	    int width = stdin.nextInt();

	    String event = stdin.next();

	    while(!event.equals("X")) {

		if(event.equals("A")) {
		    int id = stdin.nextInt();
		    int bwidth = stdin.nextInt();

		    // add the book to the shelf at the left
		    Book b = new Book(id, bwidth);
		    shelf.addFirst(b);

		    // bump the books along
		    Iterator<Book> itr = shelf.iterator();
		    Book lBook = itr.next();

		    while(itr.hasNext()) {

 			Book rBook = itr.next();
			if(lBook.end >= rBook.start) {
			    rBook.start = lBook.end + 1;
			    rBook.end = rBook.start + rBook.width - 1;
			} // if book needs to be bumped

			lBook = rBook;

		    } // while 

		    // zap any books that fall off the end
		    Book eBook = shelf.getLast();
		    while(eBook.end > width) {
			shelf.removeLast();
			eBook = shelf.getLast();
		    }

		} // if add

		else if(event.equals("R")) {
		    int id = stdin.nextInt();
		    Book b = new Book(id, -1);

		    shelf.remove(b);

		} // if remove

		event = stdin.next();

	    } // while we're still going

	    System.out.println("Case " + caseNo + ":");

	    for(Book b : shelf)
		System.out.println(b);
	    
	    System.out.println();

	} // for each case

    } // run

    public static void main(String[] args)
    {
	Prob07 p = new Prob07();
	p.run();

    } // main

    private class Book {
	int start, end, id, width;
	
	public Book(int id, int width) {
	    this.id = id;
	    this.width = width;

	    start = 1;
	    end = width;

	} // constructor

	public boolean equals(Object o) {
	    if(o instanceof Book)
		if(((Book) o).id == id)
		    return true;

	    return false;

	} // equals

	public String toString() {
	    return String.valueOf(id);

	} // toString

    } // public class Book

} // public class Prob07
