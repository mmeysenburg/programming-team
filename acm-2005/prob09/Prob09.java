import java.util.*;

public class Prob09
{
    public static void main(String[] args)
    {
	Scanner stdin = new Scanner(System.in);

	double r1 = stdin.nextDouble();
	double r2 = stdin.nextDouble();
	double r3 = stdin.nextDouble();

	int caseNo = 1;

	while(r1 > 0 && r2 > 0 && r3 > 0) {
	    
	    System.out.println("Case " + caseNo++ + ": radius = ");
	    System.out.println();

	    r1 = stdin.nextDouble();
	    r2 = stdin.nextDouble();
	    r3 = stdin.nextDouble();
	} // while

    } // main

} // public class Prob09
