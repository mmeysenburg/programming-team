import java.util.*;

public class Prob01
{
    public static void printWins(int p, int n)
    {
	int howMany = 0;
	int played = 1, wins = 0;

	while(howMany < n) {
	    int ip = (int)( ((double)wins) / played * 100 );
	    if(ip == p) {
		System.out.println("\t" + wins + 
				   " games out of " + 
				   played);
		howMany++;
	    }
	    if(ip <= p) {
		wins++;
	    }
	    else {
		played++;
		wins = 0;
	    }
	}
    }

    public static void main(String[] args)
    {
	int P, N, caseNo = 1;
	Scanner stdin = new Scanner(System.in);
	
	P = stdin.nextInt();
	N = stdin.nextInt();
	
	while(P != 0 || N != 0) {
	    System.out.println("Case " + caseNo++ + ": p = " + P +
			       ", n = " + N);
	    
	    printWins(P, N);
	    
	    P = stdin.nextInt();
	    N = stdin.nextInt();
	}
    }
}
