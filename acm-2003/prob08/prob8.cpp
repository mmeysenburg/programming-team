// solution for problem 8, 2003 ACM programming contest
//
// Mark M. Meysenburg, 11/11/2004

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <limits>
using namespace std;

int costToCompany(int N, int hiringCost, int firingCost, int monthlySalary,
		  int workersNeeded[])
{
  // initial costs for hiring our first month's worth of workers
  int cost = hiringCost * workersNeeded[0];

  // now do the middle months
  for(int month = 0; month < N - 1; month++) {

    int hCosts = 0, fCosts = 0;

    // are we hiring?
    if(workersNeeded[month] < workersNeeded[month + 1]) {

      hCosts = (workersNeeded[month + 1] - workersNeeded[month]) *
	hiringCost;
      
    } // hiring

    // are we firing?
    else if(workersNeeded[month] > workersNeeded[month + 1]) {

      fCosts = (workersNeeded[month] - workersNeeded[month + 1]) *
	firingCost;

    } // firing

    cost += hCosts + fCosts + workersNeeded[month] * monthlySalary;

  } // middle months

  // last month cost, salary only
  cost += workersNeeded[N - 1] * monthlySalary;

  return cost;

} // costToCompany

int recurse(int N, int h, int f, int m, int wn[], int max, int mo)
{
  if(mo >= N)
    return INT_MAX;

  int cost = costToCompany(N, h, f, m, wn);

  for(int i = wn[mo]; i < max; i++) {
    int *temp = new int[N];
    for(int j = 0; j < N; j++)
      temp[j] = wn[j];
    temp[mo] = i;

    int c = recurse(N, h, f, m, temp, max, mo + 1);

    if(c < cost)
      cost = c;

    delete [] temp;
  } // for

  return cost;
}

int maxWorkers(int N, int workersNeeded[])
{
  int max = workersNeeded[0];

  for(int i = 1; i < N; i++)
    if(workersNeeded[i] > max)
      max = workersNeeded[i];

  return max;

} // maxWorkers

int main()
{
  int N, caseNum = 1;

  cin >> N;

  while(N != 0) {

    int hiringCost, firingCost, monthlySalary;

    cin >> hiringCost >> monthlySalary >> firingCost;

    int* workersNeeded = new int[N];

    for(int i = 0; i < N; i++)
      cin >> workersNeeded[i];

    // start varying each of the worker numbers, in turn, from
    // their original value up to the maximum number ever required
    int maxNumberRequired = maxWorkers(N, workersNeeded);

    int minimumCost = INT_MAX;

    int c = recurse(N, hiringCost, firingCost, monthlySalary, workersNeeded,
		    maxNumberRequired, 0);

    if(c < minimumCost)
      minimumCost = c;

    cout << "Case " << caseNum++ << ", cost = $" << minimumCost << endl;

    delete [] workersNeeded;

    cin >> N;

  } // while there are still cases

  return 0;
} // main
