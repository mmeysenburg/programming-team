// prob 7, 2003 acm contest
#include <fstream>
#include <iostream>
#include <cmath>
using namespace std;

int main()
{
   
  int N;
  int i = 1;
  
  cin >> N;
  
  while(N != 0) {

    cout << "Case " << i++ << ", N = " << 
      N << ", # of different labelings = " <<
      pow((double)N, (double)(N - 2)) << endl;

    cin >> N;
  }
   
  return 0;
}
