import java.util.*;

/**
 * Solution to problem 7 from the 2003 ACM programming
 * contest.
 * 
 * @author Mark M. Meysenburg
 * @version 11/10/2004
 */
public class prob7
{
    public static void main(String[] args)
    {
	Scanner stdin = new Scanner(System.in);

	double n = stdin.nextDouble();
	int i = 1;

	while(n != 0.0) {
	    System.out.println("Case " + i + ", " +
			       "N = " + (int)n + ", " +
			       "# of different labelings = " +
			       (int)Math.pow(n, n - 2));
	    i++;
	    n = stdin.nextDouble();

	} // while
    }
}