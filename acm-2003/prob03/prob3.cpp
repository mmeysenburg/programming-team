#include <iostream>
#include <fstream>
using namespace std;

int main()
{
	int n, cases = 1, stars;

	cin >> n;

	while(n != 0)
	{
		int *points = new int[n];
		for(int i = 0; i < n; i++)
			points[i] = i + 1;

		stars = 0;

		cout << "Case " << cases++ << ", n = " << n
			<< ", unique stars = " << stars << endl;

		delete [] points;
		in >> n;
	}

	return 0;
}