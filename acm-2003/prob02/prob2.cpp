// a solution to problem 2 from the 2003 ACM programming contest
// Mark M. Meysenburg

#include <iostream>
#include <queue>
#include <set>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

// class representing a cost / length pair. Cost is cost of making
// the palindrome, length is the length of the palindrome.
// Has operators to support comparisons, so objects of this type
// can be placed in a priority queue. Also has a friend output
// operator, for debugging purposes.
class mypair
{
public:
	mypair(int c = 0, int l = 0) : cost(c), length(l) { }
	bool operator< (const mypair& rhs) const;
	bool operator> (const mypair& rhs) const;
	bool operator== (const mypair& rhs) const;
	int cost;
	int length;

	friend ostream& operator<< (ostream& ostr, const mypair& rhs);
};

ostream& operator<< (ostream& ostr, const mypair& rhs)
{
	ostr << rhs.cost << ", " << rhs.length;
	return ostr;
}

bool mypair::operator< (const mypair& rhs) const
{
	if(cost > rhs.cost)
		return true;
	else if( (cost == rhs.cost) && (length < rhs.length) )
		return true;
	else
		return false;
}

bool mypair::operator> (const mypair& rhs) const
{
	if(cost < rhs.cost)
		return true;
	else if( (cost == rhs.cost) && (length > rhs.length) )
		return true;
	else
		return false;
}

bool mypair::operator== (const mypair& rhs) const
{
	return cost == rhs.cost && length == rhs.length;
}


//////////////////////////////////////////////////////////////////////////////////
// bad programmer! global variables declared below!
//////////////////////////////////////////////////////////////////////////////////
// maximum cost that would be required to make a palindrome
int maxCost;

// priority queue holding cost / length pairs; eventually,
// the least expensive cost and its length will pop out of
// the queue
priority_queue<mypair> q;

// iterative palindrome checker: return true if the string
// is a palindrome, false otherwise
bool isPal(const string& str)
{
	int l = 0, r = str.length() - 1;
	while(l <= r)
		if(str[l++] != str[r--])
			return false;

	return true;
}

// fill the priority queue with a cost / length pair for the string s,
// if s is a palindrome. Otherwise, make four recursive calls for each
// of the operations we could perform on the string (add left, add right,
// remove left, and remove right)
void mytry(const string& s, int cost)
{
	// don't go deeper if our maximum cost is met, or if our
	// string length is zero
	if( cost <= maxCost && s.length() > 0)
	{
		// OK, if s is a palindrome at this point, add the
		// cost it took to make it that way, and the length
		// of the string, as a cost / length pair to the 
		// priority queue.
		if(isPal(s))
		{
			q.push(mypair(cost, s.length()));
		}

		// if s is not a palindrome, start making things that might
		// be palindromes via the operators that we have
		else
		{
			// this set holds the characters in the string... we won't have to 
			// worry about adding other digits, because making a palindrome
			// that way would take too many operations
			set<char> charset;

			// put the characters in the set
			for(int i = 0; i < s.length(); i++)
				charset.insert(s[i]);

			// now, try to make palindromes by adding a character to the left
			// or to the right side of the string
			while(!charset.empty())
			{
				// grab one of the characters, and nuke it from the set
				char c = *(charset.begin());
				charset.erase(c);

				// add the char to the left and see if it's a palindrome
				string s1 = c + s;
				mytry(s1, cost + 1);

				// ditto the right side
				s1 = s + c;
				mytry(s1, cost + 1);
			}

			// OK, now try the other operation: removing the left or right
			// character. Remove right side character first.
			string s1 = s.substr(0, s.length() - 1);
			mytry(s1, cost + 1);

			// Now do the left side.
			s1 = s.substr(1, s.length() - 1);
			mytry(s1, cost + 1);

		} // if s was not a palindrome

	} // if we're not doing a base case

} // mytry

int main()
{
	string word;
	int cases = 1;

	while(cin >> word)
	{
		maxCost = word.length() - 1;      
		mytry(word, 0);

		// top of the priority queue, q, will be the shortest cost of
		// making the word into a palindrome
		cout << "Case " << cases++ << ", sequence = " << word <<
			", cost = " << (q.top()).cost << ", length = " <<
			(q.top()).length << endl;

		// empty the priority queue
		while(!q.empty())
			q.pop();

	}

	return 0;
}
