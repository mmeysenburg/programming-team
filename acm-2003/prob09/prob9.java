import java.util.*;

/**
 * Solution for problem 9, 2003 ACM programming contest.
 *
 * @author Mark M. Meysenburg
 * @version 11/10/2004
 */
public class prob9
{
    public static void main(String[] args)
    {
	int A, B, Case = 1;
	int[] digitCounts = new int[10];
	boolean done = false;

	Scanner stdin = new Scanner(System.in);

	A = stdin.nextInt();
	B = stdin.nextInt();

	if(A > B) {
	    int t = A;
	    A = B;
	    B = t;
	}

	while(!done) {
	    for(int i = 0; i < 10; i++)
		digitCounts[i] = 0;

	    for(int i = A; i <= B; i++) {
		
		int t = i;
		while(t > 0) {
		    digitCounts[t % 10]++;
		    t /= 10;
		}
	    }

	    System.out.print("Case " + Case++ + ": ");

	    for(int i = 0; i < 10; i++)
		System.out.print(i + ":" + digitCounts[i] + " ");

	    System.out.println();

	    A = stdin.nextInt();

	    if(A == 0)
		done = true;
	    else
		B = stdin.nextInt();

	    if(A > B && !done) {
		int t = A;
		A = B;
		B = t;
	    }
	}
    }
}
