// prob 9, 2003 acm contest
#include <fstream>
#include <iomanip>
#include <iostream>
using namespace std;

int main()
{   
  // beginning and ending page number to be printed
  int A, B;
  
  // array holding counts of the digits required
  int digitCounts[10];
  
  // keep track of which case we're on
  int Case = 1;
  
  // are we there yet?
  bool done = false;
  
  cin >> A >> B;
  // bastages! A and B might not be in order!
  if(A > B) {
    int temp = A;
    A = B;
    B = temp;
  }

  // keep going as long as we have cases
  while(!done) {

    // zero out the counts
    for(int i = 0; i < 10; i++)
      digitCounts[i] = 0;
         
    // for each page in the section to be printed...
    for(int i = A; i <= B; i++) {

      // now look at each digit, working right to left
      int t = i;
      while(t > 0) {
	digitCounts[t % 10]++;
	t /= 10;
      }
    }

    // output results
    cout << "Case " << Case++ << ": ";
            
    for(int i = 0; i < 10; i++)
      cout << i << ":" << digitCounts[i] << " ";
         
    cout << endl;
      
    // get next inputs; have to read A and B separately,
    // as a single A of zero ends the program.
    cin >> A;
      
    if(A == 0)
      done = true;
    else
      cin >> B;

    if(A > B && !done) {
      int temp = A;
      A = B;
      B = temp;
    }
  }
      
  return 0;
}   

