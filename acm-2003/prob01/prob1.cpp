// prob 1, 2003 acm contest
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cmath>
#include <limits>
using namespace std;

double degreesToRadians(double d)
{
	const double PI = 3.14159265358979;
	double r = (PI * d) / 180.0;
	return r;
}

int main()
{

	// 	ifstream in("in1");
	// 	ofstream out("out1");

	// 	if(!in.is_open() || !out.is_open())
	// 	{
	// 		cerr << "file error" << endl;
	// 		exit(-1);
	// 	}

	int N, cases = 1, f1, f2;

	cin >> N;

	while(N != 0) {
		double *samples = new double[N];

		for(int i = 0; i < N; i++)
			cin >> samples[i];

		for(f1 = 400; f1 <= 600; f1++)
			for(f2 = 400; f2 <= 600; f2++) {
				double v1 = 2.0 * cos( 
					degreesToRadians( ( (f1 - f2) / 2.0 ) * 1.0 / (double) N) );
				v1 = samples[0] / v1;
				v1 = asin(v1) * 2.0;

				double f = degreesToRadians( ( (f1 - f2) / 2.0 ) * 1.0 / (double) N);

				if( v1 == f ) {
					cout << "Case " << cases++ << ", f1 = " << f1 <<
						", f2 = " << f2 << endl;
					f1 = 601;
					f2 = 601;
				}
			}

			delete [] samples;
			cin >> N;
	}

	return 0;
}   

