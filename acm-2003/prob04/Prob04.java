package prob04;

import java.text.NumberFormat;
import java.util.Scanner;

/**
 * Java solution to Problem 4 from the 2003 ACM Regional Programming Contest.
 * 
 * @author Mark M. Meysenburg
 */
public class Prob04 {

    public static void main(String[] args) {
        // NumberFormat object to control how many places to print
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        
        double initialInv,          /* initial investment amount */
                intRate,            /* interest rate */
                endValue;           /* final investment amount */
        int intervalsPerYear,       /* times per year to compound */
                caseNum = 1;        /* case number counter */

        // scanner to read from the keyboard
        Scanner stdin = new Scanner(System.in);
        
        // read first case values
        initialInv = stdin.nextDouble();
        intRate = stdin.nextDouble();
        intervalsPerYear = stdin.nextInt();

        // keep processing until the sentinel is reached
        while (initialInv != 0.0 && intRate != 0.0 && intervalsPerYear != 0) {
            endValue = initialInv;

            double monthlyRate = intRate / 100.0 / intervalsPerYear;

            for (int j = 0; j < intervalsPerYear; j++) {
                // calculate monthly gain
                double monthlyGain = endValue * monthlyRate;
                
                // AND trim off any fractional cents
                int t = (int) (monthlyGain * 100);
                monthlyGain = t / 100.0;
                
                // add monthly gain to investment
                endValue += monthlyGain;
                
            } // for j

            // output formatted results
            System.out.println("Case " + caseNum++ + ". $"
                    + nf.format(initialInv) + " at " 
                    + nf.format(intRate) + "% APR compounded "
                    + intervalsPerYear + " times yields $" 
                    + nf.format(endValue));

            // get next case numbers
            initialInv = stdin.nextDouble();
            intRate = stdin.nextDouble();
            intervalsPerYear = stdin.nextInt();
            
        } // while
        
    } // main
    
} // Prob04
