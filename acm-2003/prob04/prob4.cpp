// prob 4, 2003 acm contest
#include <fstream>
#include <iomanip>
#include <iostream>
using namespace std;

int main()
{
	double p, i, v;
	int c, num = 1;

	cin >> p >> i >> c;

	while(p != 0.0 && i != 0.0 && c != 0) {
		v = p;

		double m = i / 100.0 / c;

		for(int j = 0; j < c; j++) {
			double di = v * m;
			int t = di * 100;
			di = t / 100.0;
			v += di;
		}

		cout << fixed << setprecision(2);    
		cout << "Case " << num++ << ". $" 
			<< p << " at " << i << "% APR compounded " <<
			c << " times yields $" << v << endl;

		cin >> p >> i >> c;
	}

	return 0;
}   

