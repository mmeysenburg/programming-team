
import java.util.Scanner;

public class Prob03 {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String[] lines = new String[6];

        String line = stdin.nextLine();
        int i = 0;

        while(!line.equals("0-0-0-0-0")) {
            lines[i++] = line;
            while(i < 6) {
                lines[i++] = stdin.nextLine();
            }
            
            
            line = stdin.nextLine();
            i = 0;
            lines = new String[6];
        } // while file has data
    } // main
} // Prob03
