
import java.util.Scanner;

public class Prob01 {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int m = stdin.nextInt();
        int n = stdin.nextInt();
        int c = 1;

        while(m != 0 && n != 0) {
            long num = binomial(n + m - 2, m - 1);


            System.out.println("Case " + c + ": " + num);

            m = stdin.nextInt();
            n = stdin.nextInt();
            c++;
        }

    }

    private static long binomial(int n, int m) {
        if(n < m) {
            int t = n;
            n = m;
            m = t;
        }

        long[] b = new long[n + 1];
        b[0] = 1L;
        for(int i = 1; i <= n; ++i) {
            b[i] = 1L;
            for(int j = i - 1; j > 0; --j) {
                b[j] += b[j - 1];
            }
        }

        return b[m];
    }

}
