
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class Prob07 {

    public static void main(String[] args) {
        TreeMap<String, String> textToNum = new TreeMap<String, String>();
        TreeMap<String, String> numToText = new TreeMap<String, String>();

        buildTextToNum(textToNum);
        buildNumToText(numToText);

        Scanner stdin = new Scanner(System.in);

        String line = stdin.nextLine();
        while (!line.startsWith("*")) {
            char c = line.charAt(0);

            if (Character.isDigit(c) || c == '#') {
                // numbers to text
                StringTokenizer stk = new StringTokenizer(line, "-P");
                while(stk.hasMoreTokens()) {
                    String token = stk.nextToken();
                    System.out.print(numToText.get(token));
                }
                System.out.println("");
            } else {
                LinkedList<String> list = new LinkedList<String>();
                // text to numbers
                for (int i = 0; i < line.length(); i++) {
                    list.add(textToNum.get(Character.toString(line.charAt(i))));
                }
                System.out.print(list.get(0));
                for(int i = 1; i < list.size(); i++ ) {
                    if(insertPause(list.get(i-1), list.get(i))) {
                        System.out.print("P-" + list.get(i));
                    } else {
                        System.out.print("-" + list.get(i));
                    }
                }
                System.out.println();
            }

            line = stdin.nextLine();
        }
    }

    private static void buildTextToNum(TreeMap<String, String> textToNum) {
        textToNum.put("A", "2");
        textToNum.put("B", "22");
        textToNum.put("C", "222");
        textToNum.put("D", "3");
        textToNum.put("E", "33");
        textToNum.put("F", "333");
        textToNum.put("G", "4");
        textToNum.put("H", "44");
        textToNum.put("I", "444");
        textToNum.put("J", "5");
        textToNum.put("K", "55");
        textToNum.put("L", "555");
        textToNum.put("M", "6");
        textToNum.put("N", "66");
        textToNum.put("O", "666");
        textToNum.put("P", "7");
        textToNum.put("Q", "77");
        textToNum.put("R", "777");
        textToNum.put("S", "7777");
        textToNum.put("T", "8");
        textToNum.put("U", "88");
        textToNum.put("V", "888");
        textToNum.put("W", "9");
        textToNum.put("X", "99");
        textToNum.put("Y", "999");
        textToNum.put("Z", "9999");
        textToNum.put(" ", "#");
    }

    private static void buildNumToText(TreeMap<String, String> numToText) {
        numToText.put("2", "A");
        numToText.put("22", "B");
        numToText.put("222", "C");
        numToText.put("3", "D");
        numToText.put("33", "E");
        numToText.put("333", "F");
        numToText.put("4", "G");
        numToText.put("44", "H");
        numToText.put("444", "I");
        numToText.put("5", "J");
        numToText.put("55", "K");
        numToText.put("555", "L");
        numToText.put("6", "M");
        numToText.put("66", "N");
        numToText.put("666", "O");
        numToText.put("7", "P");
        numToText.put("77", "Q");
        numToText.put("777", "R");
        numToText.put("7777", "S");
        numToText.put("8", "T");
        numToText.put("88", "U");
        numToText.put("888", "V");
        numToText.put("9", "W");
        numToText.put("99", "X");
        numToText.put("999", "Y");
        numToText.put("9999", "Z");
        numToText.put("#", " ");
    }

    private static boolean insertPause(String a, String b) {
        return a.startsWith(Character.toString(b.charAt(0)));
    }
}
