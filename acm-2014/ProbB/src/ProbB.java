
import java.util.LinkedList;
import java.util.Scanner;

public class ProbB {
    private static Node head = null;

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int caseNo = 1;
        
        while(stdin.hasNextInt()) {
            LinkedList<Integer> inputList = new LinkedList<>();
            int n = stdin.nextInt();
            while(n > 0) {
                inputList.add(n);
                n = stdin.nextInt();
            } // while list
            
            System.out.println("Case " + caseNo + 
                    ": ");
            caseNo++;
        } // while input
    } // main
} // ProbB
