
import java.util.Scanner;

public class ProbD {

    public static void main(String[] args) {
        Scanner stdIn = new Scanner(System.in);
        int n, T, caseNo = 1;
        
        while(stdIn.hasNextInt()) {
            n = stdIn.nextInt();
            T = stdIn.nextInt();
            
            int[] jobs = new int[n];
            for(int i = 0; i < n; i++) {
                jobs[i] = stdIn.nextInt();
            }
            
            int timeSoFar = 0;
            int numJobs = 0;
            for(int i = 0; i < n; i++) {
                timeSoFar += jobs[i];
                if(timeSoFar <= T) {
                    numJobs++;
                }
            }
            
            System.out.println("Case " + caseNo + ": " +
                    numJobs);
            caseNo++;
        } // while input
    } // main
    
} // ProbD
