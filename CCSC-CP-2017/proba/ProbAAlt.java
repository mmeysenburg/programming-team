
import java.util.HashMap;
import java.util.Scanner;

/**
 * Solution to CCSC-CP 2017 programming contest problem A: 
 * "Fibonacci Numbering System"
 *
 * @author Mark M. Meysenburg
 * @version 04/01/2017
 */
public class ProbAAlt {

    // array to hold first 26 Fibonacci numbers; compute once, and then
    // refer to the array after that -- the 24th Fibonacci number is 121393,
    // and that's the second one larger than 2^16 = 65536, so storing up 
    // through it is plenty
    private static final int[] fibs = new int[25];

    // map to store answers we've already discovered
    private static final HashMap<Integer, Integer> map = new HashMap<>();

    /**
     * Compute the value of the nth Fibonacci number, iteratively.
     *
     * @param n
     * @return Fib(n), the nth Fibonacci number
     */
    private static int fib(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            int f0 = 1;
            int f1 = 1;
            for (int i = 2; i <= n; i++) {
                int f2 = f1 + f0;
                f0 = f1;
                f1 = f2;
            }

            return f1;
        }
    }

    /**
     * Convert a number in Fibonacci representation to decimal.
     *
     * @param s String holding the Fibonacci representation, e.g., 1101 is 
     * one representation of 6.
     *
     * @return Decimal equivalent of the representation.
     */
    private static int stringToValue(String s) {
        int val = 0;

        int j = 0;
        // work from right to left in the string
        for (int i = s.length() - 1; i >= 0; i--) {
            char c = s.charAt(i);
            // if the jth character is one, add the jth Fibonacci number
            // to the value
            val += c == '1' ? fibs[j] : 0;
            j++;
        }

        return val;
    }

    /**
     * Count the number of possible Fibonacci representations for a given
     * decimal number.
     *
     * @param n Decimal number to count representations for
     *
     * @return Number of Fibonacci representations for n
     */
    private static int count(int n) {
        // if we've computed the value before, return that value (this will
        // only matter if the input is many, many numbers)
        if (map.containsKey(n)) {
            return map.get(n);
        } else {
            // normal case: call the recursive helper method and save the
            // answer. 
            int num = count(n, 0, 0);
            map.put(n, num);
            return num;
        }
    }

    /**
     * Recursive helper method for count. Add or do not add the ith 
     * Fibonacci number to the sum.
     *
     * @param n Decimal number to count representations for
     * @param i Index of the ith Fibonacci number
     * @param sum Current sum of all the included Fibonacci numbers in the
     * potential representation
     *
     * @return Number of representations possible given n, i, and sum.
     */
    private static int count(int n, int i, int sum) {
        // newSum is what our sum would be if the ith Fibonacci number is
        // included
        int newSum = sum + fibs[i];
        
        // greater than target? No need to go further on this branch
        if (newSum > n) {
            return 0;
        } else {
            // equal to target? ...
            if (newSum == n) {
                // ... 1 solution plus all the solutions that *don't* use
                // the ith Fibonacci number
                return 1 + count(n, i + 1, sum);
            } else {
                // general case: add number of solutions where we use 
                // the ith Fibonacci and the number where we don't
                return count(n, i + 1, sum) + count(n, i + 1, newSum);
            }
        }
    }

    /**
     * Application entry point.
     * 
     * @param args Command-line arguments; ignored by this application. 
     */
    public static void main(String[] args) {

        // fill the Fibonacci number table
        for (int i = 0; i < fibs.length; i++) {
            fibs[i] = fib(i);
        }

        Scanner stdin = new Scanner(System.in);

        int caseNum = 1;
        String fib = stdin.nextLine().trim();
        int val = Integer.parseInt(fib, 2);
        while (val != 0) {

            System.out.print("Case " + caseNum + ": ");
            int v = stringToValue(fib);
            System.out.print(v + " ");

            // find first Fibonacci number less than or equal to the value
            int i;
            for (i = fibs.length - 1; i >= 0; i--) {
                if (fibs[i] <= v) {
                    break;
                }
            }
            System.out.println(count(v));

            caseNum++;
            fib = stdin.nextLine().trim();
            val = Integer.parseInt(fib, 2);
        }
    }
}
