
import java.util.Scanner;

/**
 * Solution to CCSC-CP 2017 programming contest problem A:
 * "Fibonacci Numbering System"
 * 
 * @author Mark M. Meysenburg
 * @version 04/01/2017
 */
public class ProbA {

    // array to hold first 26 Fibonacci numbers; compute once, and then
    // refer to the array after that
    private static int[] fibs = new int[27];

    /**
     * Compute the value of the nth Fibonacci number, iteratively.
     * 
     * @param n
     * @return Fib(n), the nth Fibonacci number
     */
    private static int fib(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            int f0 = 1;
            int f1 = 1;
            for (int i = 2; i <= n; i++) {
                int f2 = f1 + f0;
                f0 = f1;
                f1 = f2;
            }

            return f1;
        }
    }

    /**
     * Convert a number in Fibonacci representation to decimal.
     * 
     * @param s String holding the Fibonacci representation, e.g., 1101
     * is one representation of 6.
     * 
     * @return Decimal equivalent of the representation. 
     */
    private static int stringToValue(String s) {
        int val = 0;

        int j = 0;
        // work from right to left in the string
        for (int i = s.length() - 1; i >= 0; i--) {
            char c = s.charAt(i);
            // if the jth character is one, add the jth Fibonacci number
            // to the value
            val += c == '1' ? fibs[j] : 0;
            j++;
        }

        return val;
    }

    /**
     * Recursive helper function for determining number of representations
     * of an integer.
     * 
     * @param fib1 Fib(fib1)
     * @param fib2 Fib(fib2)
     * @param n Target we're trying to count for
     * @return Number of Fibonacci representations of n arising from 
     * Fib(fib1) and Fib(fib2)
     */
    private static int count(int fib1, int fib2, int n) {
        // add the two numbers
        int fib = fib1 + fib2;
        
        // exceeded target value? Return zero and prune this branch
        if (fib > n) {
            return 0;
        }
        
        // otherwise keep going
        if (n == fib) {
            // we found one sum that makes it work, so add it 
            return 1 + count(fib2, fib, n - fib) + count(fib2, fib, n);
        } else {
            // otherwise just explore the two possibilities 
            return count(fib2, fib, n - fib) + count(fib2, fib, n);
        }
    }

    /**
     * Determine the number of Fibonacci representations of n. Relies on this
     * theorem: For n >= 1 and Fib(i) is the largest Fibonacci number such 
     * that Fib(i) <= n. Then, a representation of n as a sum of distinct
     * Fibonacci numbers must contain either Fib(i) or Fib(i - 1), but not
     * both. 
     * 
     * @param n Value to count Fibonacci representations for.
     * 
     * @return Number of Fibonacci representations for n
     */
    public static int count(int n) {
        return count(1, 0, n);
    }

    public static void main(String[] args) {

        // fill the Fibonacci number table
        for (int i = 0; i < fibs.length; i++) {
            fibs[i] = fib(i);
        }

        Scanner stdin = new Scanner(System.in);

        int caseNum = 1;
        String fib = stdin.nextLine().trim();
        int val = Integer.parseInt(fib, 2);
        while (val != 0) {

            System.out.print("Case " + caseNum + ": ");
            int v = stringToValue(fib);
            System.out.print(v + " ");
            System.out.println(count(v));

            caseNum++;
            fib = stdin.nextLine().trim();
            val = Integer.parseInt(fib, 2);
        }
    }
}
