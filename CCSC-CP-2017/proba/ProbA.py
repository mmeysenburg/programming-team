
def fib(n):
	if n == 0 or n == 1:
		return 1
	else:
		f0 = 1
		f1 = 1
		for i in range(2, n + 1):
			f2 = f1 + f0
			f0 = f1
			f1 = f2
		return f1

def stringToValue(s):
	val = 0
	j = 0
	for i in range(len(s) - 1, -1, -1):
		if s[i] == '1':
			val = val + fibs[j]
		j = j + 1
	return val
	
'''
see http://stackoverflow.com/questions/22156568/calculate-the-number-of-representations-of-a-number-as-a-sum-of-fibonacci-number
memo = {}
def F(n, x = True):
	if n == 0: 
		return 1
	if (n, x) in memo:
		return memo[n, x]
	i = 1
	a, b = 1, 1
	while b + a <= n:
		a, b = b, a + b
	memo[n, x] = (F(n - b) if x else 0) + F(n - a, n - a < a)
	return memo[n, x]
'''
def nfibhelper(fibm1, fibm2, n):
	fib = fibm1 + fibm2
	if fib > n:
		return 0
	r = 0
	if n == fib:
		r += 1
	return r + nfibhelper(fibm2, fib, n - fib) + nfibhelper(fibm2, fib, n)
	
def F(n):
	return nfibhelper(1, 0, n)
	
fibs = []
for i in range(27):
	fibs.append(fib(i))

line = input()
caseNum = 1;
while line != "0":
	v = stringToValue(line)
	print("Case ", caseNum, ": ", v, " ", F(v), sep='')
	caseNum = caseNum + 1
	line = input()
