def fib(n):
	if n == 0 or n == 1:
		return 1
	else:
		f0 = 1
		f1 = 1
		for i in range(2, n + 1):
			f2 = f1 + f0
			f0 = f1
			f1 = f2
	return f1
	
for i in range(0, 33):
	print(i, fib(i))