
import java.util.*;

public class ProbF {

    public static void main(String[] args) {
        ProbF f = new ProbF();
    }

    public ProbF() {
        Scanner stdin = new Scanner(System.in);

        int caseNum = 1;
        String line = stdin.nextLine().trim();
        while (!line.equals("0")) {
            System.out.println("Case " + caseNum + ": ");

            Scanner scanLine = new Scanner(line);
            int n = scanLine.nextInt();
            XY[] list = new XY[n];
            for (int i = 0; i < n; i++) {
                list[i] = new XY(scanLine.nextInt(), scanLine.nextInt());
            }

            int minX = Integer.MAX_VALUE;
            int maxX = Integer.MIN_VALUE;
            int minY = Integer.MAX_VALUE;
            int maxY = Integer.MIN_VALUE;

            for (int i = 0; i < n; i++) {
                if (list[i].x < minX) {
                    minX = list[i].x;
                }
                if (list[i].x > maxX) {
                    maxX = list[i].x;
                }
                if (list[i].y < minY) {
                    minY = list[i].y;
                }
                if (list[i].y > maxY) {
                    maxY = list[i].y;
                }
            }

            // arrange south side
            List<XY> south = new LinkedList<>();
            for (int i = 0; i < n; i++) {
                if (list[i].y == minY) {
                    south.add(list[i]);
                }
            }
            south.sort((XY x1, XY x2) -> (x1.x - x2.x));

            // arrange east side
            List<XY> east = new LinkedList<>();
            for (int i = 0; i < n; i++) {
                if (list[i].x == maxX) {
                    east.add(list[i]);
                }
            }
            east.sort((XY x1, XY x2) -> (x1.y - x2.y));

            // arrange north side
            List<XY> north = new LinkedList<>();
            for (int i = 0; i < n; i++) {
                if (list[i].y == maxY) {
                    north.add(list[i]);
                }
            }
            north.sort((XY x1, XY x2) -> (x2.x - x1.x));

            // arrange west side
            List<XY> west = new LinkedList<>();
            for (int i = 0; i < n; i++) {
                if (list[i].x == minX) {
                    west.add(list[i]);
                }
            }
            west.sort((XY x1, XY x2) -> (x2.y - x1.y));

            list = new XY[n + 4];
            // put back in order
            int j = 0;
            int k = 0;
            for (XY x : south) {
                x.index = k++;
                list[j++] = x;
            }
            list[j++] = new XY(maxX, minY);
            for (XY x : east) {
                x.index = k++;
                list[j++] = x;
            }
            list[j++] = new XY(maxX, maxY);
            for (XY x : north) {
                x.index = k++;
                list[j++] = x;
            }
            list[j++] = new XY(minX, maxY);
            for (XY x : west) {
                x.index = k++;
                list[j++] = x;
            }
            list[j++] = new XY(minX, minY);

            // find min distance starting point
            int minDist = Integer.MAX_VALUE;
            int minIndex = Integer.MIN_VALUE;
            for(int i = 0; i < list.length; i++) {
                if(list[i].index != -1) {
                    
                }
            }

            line = stdin.nextLine().trim();
            caseNum++;
        }
    }

    class XY {

        int x, y, index;

        public XY(int x, int y) {
            this.x = x;
            this.y = y;
            this.index = -1;
        }

        public String toString() {
            
            return "(" + x + ", " + y + ") [" + index + "]";
        }
    }
}
