
import java.util.List;
import java.util.LinkedList;
import java.util.Random;

public class MakeInput {

    private static char[] letters = {'a', 'b', 'c', 'd', 'e', 'f', 'g',
        'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
        'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
        'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
        'U', 'V', 'W', 'X', 'Y', 'Z'};

    private static String randomSpace(Random prng) {
        StringBuilder sb = new StringBuilder();
        int n = prng.nextInt(10);
        for (int i = 0; i < n; i++) {
            if (prng.nextDouble() < 0.5) {
                sb.append(' ');
            } else {
                sb.append('\t');
            }
        }

        return sb.toString();
    }

    private static String listToString(List<Character> list) {
        StringBuilder sb = new StringBuilder();

        for (char c : list) {
            sb.append(c);
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        Random prng = new Random();
        for (int n = 0; n < 100; n++) {
            int n1 = prng.nextInt(200) + 1;
            int n2 = prng.nextInt(200) + 1;

            LinkedList<Character> l1 = new LinkedList<>();
            for (int i = 0; i < n1; i++) {
                l1.addLast(letters[prng.nextInt(letters.length)]);
            }
            String s1 = listToString(l1);

            LinkedList<Character> l2 = new LinkedList<>();
            for (int i = 0; i < n2; i++) {
                l2.addLast(letters[prng.nextInt(letters.length)]);
            }
            String s2 = listToString(l2);

            int k = 0;
            StringBuilder s3 = new StringBuilder();
            if (n % 2 == 0) {
                while (!(l1.isEmpty() && l2.isEmpty())) {
                    if (prng.nextDouble() < 0.5) {
                        if (!l1.isEmpty()) {
                            s3.append(l1.removeFirst());
                        }
                    } else {
                        if (!l2.isEmpty()) {
                            s3.append(l2.removeFirst());
                        }
                    }
                }
            } else {
                while (!(l1.isEmpty() && l2.isEmpty())) {
                    if (prng.nextDouble() < 0.5) {
                        if (!l1.isEmpty()) {
                            s3.append(l1.remove(prng.nextInt(l1.size())));
                        }
                    } else {
                        if (!l2.isEmpty()) {
                            s3.append(l2.remove(prng.nextInt(l2.size())));
                        }
                    }
                }
            }
            k++;

            System.out.println(randomSpace(prng) + s1);
            System.out.println(randomSpace(prng) + s2);
            System.out.println(randomSpace(prng) + s3.toString());

        } // for n

        System.out.println(".");
    } // main
}
