
import java.util.Scanner;
/**
 * Solution to November 2004 problem 3, The Zipper. 
 * 
 * @author Mark M. Meysenburg
 * @version 04/06/2017
 */
public class prob03 {

    /**
     * Recursive method to determine if a string can be formed by combining
     * the characters of two others. The two input strings can be mixed 
     * arbitrarily, but the characters from each must stay in their original
     * order in the third string. 
     * 
     * @param s1 First input string
     * @param s2 Second input string
     * @param s3 String which may or may not be formed from s1 and s2
     * @param i1 Index of next character to consider in s1
     * @param i2 Index of next character to consider in s2
     * @param i3 Index of next character to consider in s3 -- should be 
     * s1[i1] or s2[i2]
     * @return True if s3 can be formed by combining s1 and s2, retaining the 
     * original orders in s1 and s2, false otherwise
     */
    private static boolean zipper(String s1, String s2, String s3, 
            int i1, int i2, int i3) {
        // if we've used up all of s1 or s2, see if s3 is equal to the
        // rest of the other 
        if (i1 >= s1.length()) {
            return s2.substring(i2).equals(s3.substring(i3));
            
        } else if (i2 >= s2.length()) {
            return s1.substring(i1).equals(s3.substring(i3));
            
        } else {
            // otherwise, look at first characters of each string
            char c1 = s1.charAt(i1);
            char c2 = s2.charAt(i2);
            char c3 = s3.charAt(i3);
            
            // if s3[i3] is neither s1[i1] nor s2[i2], we're done
            if ((c1 != c3) && (c2 != c3)) {
                return false;
                
            } else if ((c1 == c3) && (c2 == c3)) {
                // if s3[i3] == s1[i1] AND s3[i3] == s2[i2], then we have to
                // fork our search tree, searching in the case where we
                // use s1[i1] and in the case where we use s2[i2]
                return zipper(s1, s2, s3, i1 + 1, i2, i3 + 1)
                        || zipper(s1, s2, s3, i1, i2 + 1, i3 + 1);
                
            } else if (c1 == c3) {
                // if just s3[i3] == s1[i1], recurse on that branch
                return zipper(s1, s2, s3, i1 + 1, i2, i3 + 1);
                
            } else {
                // and if just s3[i3] == s2[i2], recurse on that branch
                return zipper(s1, s2, s3, i1, i2 + 1, i3 + 1);
            }
        }
    }

    /**
     * Application entry point. 
     * 
     * @param args Command-line arguments; ignored by this application. 
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int caseNum = 1;
        String line1 = stdin.nextLine().trim();
        while (!line1.equals(".")) {
            String line2 = stdin.nextLine().trim();
            String line3 = stdin.nextLine().trim();

            String ans = zipper(line1, line2, line3, 0, 0, 0) ? "yes" : "no";

            System.out.printf("Case %d: %s\n", caseNum, ans);
            caseNum++;

            line1 = stdin.nextLine().trim();
        } // while
    } // main
} // prob03
