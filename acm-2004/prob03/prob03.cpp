#include <iostream>
#include <string>
using namespace std;

// C++ solution to 2004 ACM programming contest problem 3, "The Zipper"
//
// Mark M. Meysenburg
// 11/18/2004
int main()
{
  int caseNum = 1;
  string s1, s2, s3;

  cin >> s1;

  while(s1 != ".") {
    cin >> s2 >> s3;

    int i1 = 0, i2 = 0, i3 = 0;
    bool works = true;

    while(i1 < s1.length() && i2 < s2.length() && i3 < s3.length()) {
      char c1 = s1[i1], c2 = s2[i2], c3 = s3[i3];
      if(c3 != c1 && c3 != c2) {
	works = false;
	break;
      }
      else if(c1 == c3) {
	i1++;
	i3++;
      }
      else if(c2 == c3) {
	i2 ++;
	i3++;
      }
    }

    cout << "Case " << caseNum++ << ": " <<
      (works ? "yes" : "no") << endl;

    cin >> s1;

  } // while

} // main
