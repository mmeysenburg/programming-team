import java.util.*;

/**
 * Solution to 2004 ACM programming contest problem 3, "The Zipper"
 *
 * @author Mark M. Meysenburg
 * @version 11/13/2004
 */
public class prob03Old
{
    public static void main(String[] args)
    {
	Scanner stdin = new Scanner(System.in);
	int caseNum = 1;

	String s1 = stdin.next();
	while(!s1.equals(".")) {
	    String s2 = stdin.next();
	    String s3 = stdin.next();

	    int i1 = 0, i2 = 0, i3 = 0;
	    boolean works = true;
	    // next character of s3 must be the next character of
	    // either s1 or s2. If it isn't, we're done and the answer
	    // is no. If it is, increment the index of s3 and either s1
	    // or s2.
	    while(i1 < s1.length() && i2 < s2.length() && i3 < s3.length()) {
		char c1 = s1.charAt(i1);
		char c2 = s2.charAt(i2);
		char c3 = s3.charAt(i3);
		if(c3 != c1 && c3 != c2) {
		    works = false;
		    break;
		}
		else if(c1 == c3) {
		    i1++;
		    i3++;
		}
		else {
		    i2++;
		    i3++;
		}
	    } // while we're still working

	    String ans = "yes";
	    if(!works)
		ans = "no";
	    System.out.println("Case " + caseNum++ + ": " +
			       ans);

	    s1 = stdin.next();
	} // while there are still input cases
    }
}
