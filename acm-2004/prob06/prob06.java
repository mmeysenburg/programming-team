import java.util.*;

public class prob06
{

    public static void main(String[] args)
    {
	Scanner stdin = new Scanner(System.in);
	int N, M, caseNum = 1;

	N = stdin.nextInt();
	M = stdin.nextInt();

	while(N != 0 && M != 0) {
	    char[][] map = new char[N][M];
	    for(int i = 0; i < N; i++) {
		String line = stdin.next().trim();
		for(int j = 0; j < M; j++)
		    map[i][j] = line.charAt(j);
	    }

	    for(int i = 0; i < N; i++) {
		for(int j = 0; j < M; j++)
		    System.out.print(map[i][j]);
		System.out.println();
	    }

	    System.out.println("\n");

	    N = stdin.nextInt();
	    M = stdin.nextInt();
	} // while there are still cases
    } // main

} // public class prob06
