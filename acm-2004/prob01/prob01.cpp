#include <iostream>
#include <set>
#include <sstream>
#include <string>
using namespace std;

// C++ solution to problem one, "Breaking a Dollar," from the 2004 ACM
// programming contest.
//
// Mark M. Meysenburg
// 11/18/2004

// make a string representation of an array of ints
string arrayToString(const int* a, int n)
{
  ostringstream s;

  s << '[';
  for(int i = 0; i < n - 1; i++)
    s << a[i] << ", ";
  s << a[n - 1] << ']';

  return s.str();
}

// recursive function to determine the number of ways a set of
// coin denominations could sum to 100, given a current number
// of each coin to start with
int recurse(const int* denoms, int* nums, int n, set<string>* repeatSet)
{
  // calculate the value selected by this combination
  int val = 0;
  for(int i = 0; i < n; i++)
    val += denoms[i] * nums[i];

  // if we're spot on 100, return a 1 to be counted in the number
  // of ways this can happen
  if(val == 100)
    return 1;

  // otherwise, see if we should go deeper in the recursion
  int num = 0;
  for(int i = 0; i < n; i++) {

    // only go deeper if adding one of a specified coin will not put
    // us over the top
    if(val + denoms[i] <= 100) {

      nums[i] += 1;

      // see if we've already visited this node of the recursion tree
      pair<set<string>::iterator, bool> result = 
	repeatSet->insert(arrayToString(nums, n));

      if(result.second == true)
	num += recurse(denoms, nums, n, repeatSet);

      nums[i] -= 1;

    } // if

  } // for

  return num;

} // recurse


// main function
int main()
{
  int N, caseNum = 1;

  cin >> N;
  while(N != -1) {

    int* denoms = new int[N];
    int* nums = new int[N];

    // read in coin denominations
    for(int i = 0; i < N; i++) {
      cin >> denoms[i];
      nums[i] = 0;
    }

    // put the denominations in ascending order
    for(int pass = 0; pass < N - 1; pass++)
      for(int j = 0; j < N - 1; j++)
	if(denoms[j] > denoms[j + 1]) {
	  int hold = denoms[j];
	  denoms[j] = denoms[j + 1];
	  denoms[j + 1] = hold;
	}

    // allocate the set for detecting repeated cases
    set<string>* repeatSet = new set<string>();
    int num = recurse(denoms, nums, N, repeatSet);
    cout << "Case " << caseNum++ << ": " <<
      num << " combinations of coins" << endl << endl;

    // deallocate this case's memory
    delete [] denoms;
    delete [] nums;
    delete repeatSet;

    // get next case N
    cin >> N;

  } // while there are still cases

  return 0;

} // main
