import java.util.*;

/**
 * Java 1.5 solution to problem one, "Breaking a Dollar," from the 2004 ACM
 * programming contest.
 *
 * @author Mark M. Meysenburg
 * @version 11/18/2004
 */
public class prob01
{
    // set to keep track of nodes we've visited
    private static TreeSet<String> set = new TreeSet<String>();

    /**
     * Convert an integer array to a string representation.
     *
     * @param a Integer array to convert.
     * @return String, like "[a0, a1, ..., a(n-1)]"
     */
    private static String arrayToString(final int[] a)
    {
	StringBuffer s = new StringBuffer();
	s.append('[');
	for(int i = 0; i < a.length - 1; i++) {
	    s.append(a[i]);
	    s.append(", ");
	}
	s.append(a[a.length - 1]);
	s.append(']');

	return s.toString();
    }

    /**
     * Recursive method, to determine the number of ways a set of
     * coin denominations could sum to 100, given a current number
     * of each coin we start with.
     *
     * @param denoms Integer array of the coin denomination values,
     * in sorted ascending order.
     * @param nums Integer array of how many of each of the coins we
     * have to start with.
     * @return The number of ways we can make 100, with the specified
     * denominations, starting with the initial number of each.
     */
    private static int recurse(final int[] denoms, int[] nums)
    {
	// how much do our current selection of coins total to?
	int val = 0;
	for(int i = 0; i < denoms.length; i++)
	    val += denoms[i] * nums[i];

	// if this selection totals to 100, don't go any deeper
	if(val == 100)
	    return 1;

	// if we haven't totaled 100 yet, try to add one of the coins,
	// one at a time, and go deeper in the recursion
	int num = 0;
	for(int i = 0; i < denoms.length; i++) {
	    // don't even try if adding the coin will put us over the top
	    if(val + denoms[i] <= 100) {
		nums[i] += 1;
		// only go deeper if we haven't evaluated this combination
		// yet
		if(set.add(arrayToString(nums)))
		    num += recurse(denoms, nums);
		nums[i] -= 1;
	    } // if
	} // for

	return num;
    }

    /**
     * Application entry point.
     *
     * @param args Command line arguments, ignored by this application.
     */
    public static void main(String[] args)
    {
	Scanner stdin = new Scanner(System.in);
	int N = stdin.nextInt();
	int caseNum = 1;

	while(N != -1) {

	    int[] denoms = new int[N]; // holds denomination of each coin
	    int[] nums = new int[N]; // holds how many of each coin

	    // read in the coin denominaions
	    for(int i = 0; i < N; i++) {
		denoms[i] = stdin.nextInt();
		nums[i] = 0;
	    }

	    // of course, there will be a case where the denominations
	    // aren't in ascending order, so sort the array before we
	    // continue
	    Arrays.sort(denoms);

	    // clear the set of repeated nodes
	    set.clear();

	    int num = recurse(denoms, nums);
	    System.out.println("Case " + caseNum++ + ": " +
			       num + " combinations of coins");
	    System.out.println();

	    // read in the next N
	    N = stdin.nextInt();

	} // while there are more cases

    } // main

} // public class prob01
