import java.text.*;
import java.util.*;

/**
 * Java 1.5 solution to problem 5 from the 2004 ACM programming contest,
 * "Stacking Cylinders."
 *
 * @author Mark M. Meysenburg
 * @version 12/2/2004
 */
public class prob05
{
    public static void main(String[] args)
    {
	Scanner stdin = new Scanner(System.in);
	NumberFormat formatter = NumberFormat.getInstance();
	formatter.setMinimumFractionDigits(4);
	formatter.setMaximumFractionDigits(4);
	int N, caseNum = 1;

	N = stdin.nextInt();
	while(N != 0) {
	    double[] x = new double[N];
	    double[] y = new double[N];
	    for(int i = 0; i < N; i++) {
		x[i] = stdin.nextDouble();
		y[i] = 1.0;
	    }

	    Arrays.sort(x);

	    while(N > 1) {
		double[] newX = new double[N - 1];
		double[] newY = new double[N - 1];

		for(int i = 0; i < N - 1; i++) {
		    double deltaX = x[i + 1] - x[i];
		    double deltaY = y[i + 1] - y[i];
		    double r = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
		    double theta = Math.asin(deltaY / r);
		    if(deltaY < 0)
			theta *= -1;
		    double theta1 = Math.asin(Math.sqrt(4 - ((r * r) / 4)) / 2)
			- theta;
		    newX[i] = 2 * Math.cos(theta1) + x[i];
		    newY[i] = 2 * Math.sin(theta1) + y[i];
		} // for

		x = new double[N - 1];
		y = new double[N - 1];
		for(int i = 0; i < N - 1; i++) {
		    x[i] = newX[i];
		    y[i] = newY[i];
		}

		N--;
	    } // upper level loop

	    System.out.println("Case " + caseNum++ + ": " + 
			       formatter.format(x[0]) + " " + 
			       formatter.format(y[0]));

	    N = stdin.nextInt();

	} // while there are more cases

    } // main

} // public class prob05