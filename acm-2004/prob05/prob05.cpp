#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;

// C++ solution to problem 5 from the 2004 ACM programming contest, "Stacking Cylinders"
//
// mmm, 12/02/2004

int compare(const void* arg1, const void* arg2)
{
  double x = *((double*)arg1);
  double y = *((double*)arg2);
  if(x < y)
    return -1;
  if(x == 0) 
    return 0;
  return 1;
}

int main()
{
  int N, caseNum = 1;

  cin >> N;
  while(N != 0) {
    double* x = new double[N];
    double* y = new double[N];
    for(int i = 0; i < N; i++) {
      cin >> x[i];
      y[i] = 1.0;
    }
    
    // sort the x array
    qsort((void*)x, N, sizeof(double), compare);
    
    // now start calculating the upper levels
    while(N > 1) {
      double *newX = new double[N - 1];
      double *newY = new double[N - 1];
      for(int i = 0; i < N - 1; i++) {
// 	newX[i] = (x[i] + x[i + 1]) / 2.0;
// 	double deltaX = x[i + 1] - x[i];
// 	newY[i] = y[i] + sqrt(4.0 - ((deltaX * deltaX) / 4.0));
	double deltaX = x[i + 1] - x[i];
	double deltaY = y[i + 1] - y[i];
	double r = sqrt(deltaX * deltaX + deltaY * deltaY);
	double theta = asin(deltaY / r);
	if(deltaY < 0)
	  theta *= -1;
	double theta1 = asin(sqrt(4 - ((r * r) / 4)) / 2.0) - theta;
	newX[i] = 2 * cos(theta1) + x[i];
	newY[i] = 2 * sin(theta1) + y[i];
      }
      
      // lose the old arrays, reallocate, copy over, and lose the temps
      delete [] x;
      delete [] y;
      x = new double[N - 1];
      y = new double[N - 1];
      for(int i = 0; i < N - 1; i++) {
	x[i] = newX[i];
	y[i] = newY[i];
      }
      delete [] newX;
      delete [] newY;
      
      N--;
		} // upper level calculations
    
    printf("Case %d: %.4f %.4f\n", caseNum++, x[0], y[0]);
    
    delete [] x;
    delete [] y;
    cin >> N;
  } // while there are more cases
  
  return 0;
}
