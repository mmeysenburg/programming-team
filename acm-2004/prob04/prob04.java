import java.util.*;

/**
 * Java 1.5 solution to problem 4, "Lenny's Lucky Lotto Lists," from the
 * 2004 ACM programming contest.
 *
 * @author Mark M. Meysenburg
 * @version 11/19/2004
 */
public class prob04
{
    // set for ignoring duplicate nodes during the recursion
    private static TreeSet<String> set = new TreeSet<String>();

    /**
     * Convert an integer array to a string representation.
     *
     * @param a Integer array to convert.
     * @return String, like "[a0, a1, ..., a(n-1)]"
     */
    private static String arrayToString(final int[] a)
    {
	StringBuffer s = new StringBuffer();
	s.append('[');
	for(int i = 0; i < a.length - 1; i++) {
	    s.append(a[i]);
	    s.append(", ");
	}
	s.append(a[a.length - 1]);
	s.append(']');

	return s.toString();
    }

    /**
     * Return the number of ways we could make lottery picks that meet
     * Lenny's requirements, given a current baseline of numbers to start
     * from. This is a recursive method that takes a LONG time for larger
     * numbers of N and M.
     *
     * @param curr Current lottery picks.
     * @param maxs Maximum values that could be used for each pick.
     * @return The number of ways Lenny could pick numbers.
     */
    private static int recurse(int[] curr, final int[] maxs) 
    {
	boolean goodSolution = true;

	// does this one work?
	for(int i = 1; i < curr.length; i++)
	    if(curr[i] < 2 * curr[i - 1] || curr[i] > maxs[i])
		goodSolution = false;

	int num = goodSolution ? 1 : 0;

	// go deeper if we can still make it work...
	if(goodSolution) {
	    for(int i = 0; i < curr.length; i++) {
		if(curr[i] + 1 <= maxs[i]) {
		    if(i == curr.length -1 || 
		       ((curr[i] + 1) * 2) <= curr[i + 1]) {
		    
			curr[i] += 1;
			if(set.add(arrayToString(curr)))
			    num += recurse(curr, maxs);
			curr[i] -= 1;
		    }
		    
		} // if
	    } // for
	} // if

	return num;
    } // recurse

    /**
     * Application entry point.
     *
     * @param args Array of Strings holding command line arguments; ignored
     * by this application.
     */
    public static void main(String[] args)
    {
	Scanner stdin = new Scanner(System.in);
	int N = stdin.nextInt();
	int M = stdin.nextInt();
	int caseNum = 1;

	while(N != 0 && M != 0) {
	    int[] mins = new int[N]; // min value for each number
	    int[] maxs = new int[N]; // max value for each number

	    int minValue = 1; 
	    int maxDivisor = 1 << (N - 1); 
	    // calculate min and max values for each number; be a 
	    // wise acre and do shifting for power of 2 calculations
	    for(int i = 0; i < N; i++) {
		mins[i] = minValue;
		maxs[i] = (int)Math.floor(M / (double)maxDivisor);
		minValue <<= 1;
		maxDivisor >>= 1;
	    }

	    // recursive approach commented out... in favor of the
	    // iterative version that counts the number of times
	    // each possible final number can appear (and therefore
	    // the total number of picks)
// 	    set.clear();
// 	    int num = recurse(mins, maxs);

	    int num = 0;
	    if(N == 1)
		num = M;
	    else {
		for(int x = mins[N - 1]; x <= maxs[N - 1]; x++)
		    for(int y = mins[N - 2]; y <= maxs[N - 2]; y++)
			if(x >= y * 2)
			    num++;
	    }

	    System.out.println("Case " + caseNum++ + ": n = " +
			       N + ", m = " + M + ", # lists = " + num);

	    // read next case
	    N = stdin.nextInt();
	    M = stdin.nextInt();

	} // while there are cases

    } // main

} // public class prob04