#include <iostream>
#include <cmath>
using namespace std;

int main()
{
  int N, M, caseNum = 1;

  cin >> N >> M;

  while(N != 0 && M != 0) {
    int* mins = new int[N];
    int* maxs = new int[N];

    int minValue = 1;
    int maxDivisor = 1 << (N - 1);

    for(int i = 0; i < N; i++) {
      mins[i] = minValue;
      maxs[i] = (int)floor(M / (double)maxDivisor);
      minValue <<= 1;
      maxDivisor >>= 1;
    }

    int num = 0;
    if(N == 1)
      num = M;
    else {
      for(int x = mins[N - 1]; x <= maxs[N - 1]; x++)
	for(int y = mins[N - 2]; y <= maxs[N - 2]; y++)
	  if(x >= y * 2)
	    num++;
    }

    cout << "Case " << caseNum++ << ": n = " << N << ", m = " 
	 << M << ", # lists = " << num << endl;

    delete [] mins;
    delete [] maxs;

    cin >> N >> M;
  }
}
