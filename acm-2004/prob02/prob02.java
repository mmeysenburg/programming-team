import java.text.*;
import java.util.*;

/**
 * Java 1.5 solution to problem 2, "Rate of Return," from the 2004 ACM
 * programming contest.
 *
 * @author Mark M. Meysenburg
 * @version 11/19/2004
 *
 */
public class prob02
{
    /**
     * Use an iterative numeric algorithm to compute the monthly
     * interest rate that would be required to equal the return on the
     * mutual fund.
     *
     * @param months Array of month numbers, starting at 1 and continuing
     * in ascending order.
     * @param depsoits Array of deposits made in each of the months 
     * specified in the months array.
     * @return Monthly interest rate that would equal the return given
     * by the mutual fund.
     */
    private static double findRate(final int[] months, final double[] deposits)
    {
	// x values represent interest rate; start out with the extremes
	double x1 = 0.0, x2 = 1.0;

	// y values represent balance at the end of the specified period
	double y1 = finalBalance(months, deposits, x1);
	double y2 = finalBalance(months, deposits, x2);

	// compute the midpoint of the line segment defined by (x1, y1) and
	// (x2, y2); the midpoint is our new guess for the interest rate
	double midx = (x1 + x2) / 2.0;
	double midy = (y1 + y2) / 2.0;

	// what are we shooting for? this is a y value, and we're
	// trying to find the x value (interest rate) that gives this
	double targetBalance = deposits[deposits.length - 1];

	// iterate until we hit the balance we're looking for
	while(midy != targetBalance) {

	    // see which half of the line segment (x1, y1), (x2, y2)
	    // our target balance lies in; then, update the endpoints
	    // of the line segment
	    if(targetBalance >= y1 &&
	       targetBalance <= midy) {
		x2 = midx;
		y2 = midy;
	    }
	    else if(targetBalance > midy &&
		    targetBalance <= y2) {
		x1 = midx;
		y1 = midy;
	    }
	    else {
		System.err.println("Something's very wrong!");
		System.err.println(x1 + " " + midx + " " + x2);
		System.err.println(y1 + " " + midy + " " + y2);
		System.exit(-1);
	    }

	    // now update the endpoint balances, and re-compute the
	    // midpoints of the line segment
	    y1 = finalBalance(months, deposits, x1);
	    y2 = finalBalance(months, deposits, x2);
	    midx = (x1 + x2) / 2.0;
	    midy = (y1 + y2) / 2.0;

	} // while we haven't hit the final balance

	return midx;

    } // findRate

    /**
     * Calculate the final balance for the mutual fund, given 
     * the deposits and a candidate effective monthly interest rate.
     *
     * @param months Array of month numbers, starting at 1 and continuing
     * in ascending order.
     * @param depsoits Array of deposits made in each of the months 
     * specified in the months array.
     * @param rate Monthly interest rate.
     * @return The final value of the mutual fund.
     */
    private static double finalBalance(final int[] months, 
				       final double[] deposits, 
				       double rate)
    {
	int lastMonth = months[months.length - 1];
	double balance = 0.0;
	int i = 0;
	for(int mo = 1; mo <= lastMonth; mo++) {
	    if(mo == months[i] && mo != lastMonth)
		balance += deposits[i++];
	    balance *= (1.0 + rate);
	}

	return balance;

    } // finalBalance

    /**
     * Application entry point.
     *
     * @param args Array of command line arguments, ignored by this 
     * application.
     */
    public static void main(String[] args)
    {
	// create a number formatter to match the required output
	NumberFormat formatter = NumberFormat.getInstance();
	formatter.setMinimumFractionDigits(5);

	Scanner stdin = new Scanner(System.in);
	int caseNum = 1, N = stdin.nextInt();

	while(N != -1) {
	    // allocate memory for the month number / deposits pairs,
	    // and read them in
	    int[] months = new int[N + 1];
	    double[] deposits = new double[N + 1];

	    for(int i = 0; i <= N; i++) {
		months[i] = stdin.nextInt();
		deposits[i] = stdin.nextDouble();
	    }

	    // calculate equivalent monthly interest rate, and
	    // display the answer for this case
	    double rate = findRate(months, deposits);
	    System.out.println("Case " + caseNum++ + ": " + 
			       formatter.format(rate));
	    System.out.println();

	    // get the next case
	    N = stdin.nextInt();

	} // while there are cases

    } // main

} // public class prob02