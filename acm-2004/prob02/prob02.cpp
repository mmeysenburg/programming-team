#include <iomanip>
#include <iostream>
#include <cstdio>
using namespace std;

double finalBalance(const int* months, const double* deposits, int n, 
		    double rate)
{
  int lastMonth = months[n];
  double balance = 0.0;
  int i = 0;
  for(int mo = 1; mo <= lastMonth; mo++) {
    if(mo == months[i] && mo != lastMonth)
      balance += deposits[i++];
    balance *= (1.0 + rate);
  }
  return balance;
}

double findRate(const int* months, const double* deposits, int n)
{
  double x1 = 0.0, x2 = 1.0;
  double y1 = finalBalance(months, deposits, n, x1);
  double y2 = finalBalance(months, deposits, n, x2);
  double midx = (x1 + x2) / 2.0;
  double midy = (y1 + y2) / 2.0;
  double targetBalance = deposits[n];

  while(midy != targetBalance) {
    if(targetBalance >= y1 && targetBalance <= midy) {
      x2 = midx;
      y2 = midy;
    }
    else if(targetBalance > midy && targetBalance <= y2) {
      x1 = midx;
      y1 = midy;
    }
    else {
      cerr << "Something is very wrong!" << endl;
      cerr << x1 << " " << midx << " " << x2 << endl;
      cerr << y1 << " " << midy << " " << y2 << endl;
      exit(-1);
    }

    y1 = finalBalance(months, deposits, n, x1);
    y2 = finalBalance(months, deposits, n, x2);
    midx = (x1 + x2) / 2.0;
    midy = (y1 + y2) / 2.0;
  }

  return midx;
}

int main()
{
  int caseNum = 1, N;

  cin >> N;

  while(N != -1) {
    int* months = new int[N + 1];
    double* deposits = new double[N + 1];

    for(int i = 0; i <= N; i++) {
      cin >> months[i];
      cin >> deposits[i];
    }

    double rate = findRate(months, deposits, N);

    printf("Case %d: %6.5f\n\n", caseNum, rate);
    caseNum++;

    delete [] months;
    delete [] deposits;

    cin >> N;
  } // while

  return 0;
}
