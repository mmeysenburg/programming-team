import java.util.*;
import java.text.DecimalFormat;

public class Prob01 {
    private static DecimalFormat df = new DecimalFormat("000000");

    public static void main(String[] args) {
	Scanner stdin = new Scanner(System.in);
	String s = stdin.nextLine();
	int i = 1;
	while(!s.equals("-1")) {
	    int ntp = 0;
	    if(isPal(Integer.toString(toInt(s)))) 
		ntp = 0;
	    else
		ntp = numToPal(s);
 	    System.out.println("Case " + i + ": " + ntp + " miles to " +
			       df.format(toInt(s) + ntp));

	    s = stdin.nextLine();
	    i++;
	} // while
    } // main

    private static int toInt(String s) {
	int i = 0;
	while(i < s.length() && s.charAt(i) == '0') {
	    i++;
	}
	s = s.substring(i);

	return s.length() == 0 ? 0 : Integer.parseInt(s);
    }

    private static int numToPal(String s) {
	int i = 0;
	int num = toInt(s);
	while(!isPal(s)) {
	    num++;
	    i++;
	    s = Integer.toString(num);
	}
	return i;
    }

    private static boolean isPal(String s) {
	int i = 0, j = s.length() - 1;
	while(i <= j) {
	    if(s.charAt(i) != s.charAt(j)) 
		return false;
	    i++;
	    j--;
	}
	return true;
    }
}