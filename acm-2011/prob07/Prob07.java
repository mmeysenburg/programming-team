import java.util.*;

public class Prob07 {

    public static void main(String[] args) {
	Scanner stdin = new Scanner(System.in);
	int k = stdin.nextInt();
	int i = 1;
	String s = "";
	while(k != -1) {
	    if(!canBeTriangular(k)) {
		s = "bad";
	    } else {
		int lo = 1, hi = 44722;
		int mid = (lo + hi) / 2;
		int bc = (int)triangular(mid);
		while(bc != k && lo <= hi) {
		    if(bc < k) 
			lo = mid + 1;
		    else
			hi = mid - 1;
		    mid = (lo + hi) / 2;
		    bc = (int)triangular(mid);
		} // while
		if(lo > hi) 
		    s = "bad";
		else
		    s = Integer.toString(mid);
	    }
	    System.out.println("Case " + i + ": " + s);
	    k = stdin.nextInt();
	    i++;
	} // while
    }

    private static boolean canBeTriangular(int n) {
	// triangular number must be divisible by 3 or have
	// remainder of 1 when divided by 9
	return (n % 3 == 0) || (n % 9 == 1);
    }

    private static long triangular(long k) {
	return (k * (k + 1)) / 2L;
    }
}
