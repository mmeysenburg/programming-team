import java.util.*;
import java.math.BigInteger;

public class Prob02 {
    private static int secPerMin = 0;
    private static int minPerHour = 0;
    private static int hoursPerDay = 0;
    private static int monthsPerYear = 0;
    private static int[] daysPerMonth = null;

    private static long secPerHour = 0L;
    private static long secPerDay = 0L;
    private static long[] secsPerMonth = null;
    private static long secPerYear = 0L;

    public static void main(String[] args) {
	String startTime = "";
	String endTime = "";

	int n = 1;

	Scanner stdin = new Scanner(System.in);
	secPerMin = stdin.nextInt();
	while(secPerMin != 0) {
	    minPerHour = stdin.nextInt();
	    hoursPerDay = stdin.nextInt();

	    monthsPerYear = stdin.nextInt();
	    daysPerMonth = new int[monthsPerYear];
	    for(int i = 0; i < daysPerMonth.length; i++) {
		daysPerMonth[i] = stdin.nextInt();
	    } // for

	    startTime = stdin.next();
	    endTime = stdin.next();

	    computeSecs();

	    BigInteger end = new BigInteger(Long.toString(timeToSec(endTime)));
	    BigInteger start = new BigInteger(Long.toString(timeToSec(startTime)));

	    System.out.println("Case " + n + ": " + (end.subtract(start)) + 
			       " seconds" );

	    n++;
	    secPerMin = stdin.nextInt();
	} // while
    }

    private static void computeSecs() {
	secPerHour = secPerMin * minPerHour;
	secPerDay = secPerHour * hoursPerDay;

	secsPerMonth = new long[monthsPerYear];
	secPerYear = 0L;
	for(int i = 0; i < secsPerMonth.length; i++) {
	    secsPerMonth[i] = secPerDay * daysPerMonth[i];
	    secPerYear += secsPerMonth[i];
	}
    }

    private static long timeToSec(String time) {
	StringTokenizer stk = new StringTokenizer(time, "/-:");
	long sum = 0L;
	String s = stk.nextToken();
	int month = Integer.parseInt(s);
	s = stk.nextToken();
	int day = Integer.parseInt(s);
	s = stk.nextToken();
	int year = Integer.parseInt(s);
	s = stk.nextToken();
	int hour = Integer.parseInt(s);
	s = stk.nextToken();
	int min = Integer.parseInt(s);
	s = stk.nextToken();
	int sec = Integer.parseInt(s);

	sum += year * secPerYear;
	for(int i = 1; i <= month - 1; i++) {
	    sum += secsPerMonth[i - 1];
	}
	sum += day * secPerDay;
	sum += hour * secPerHour;
	sum += min * secPerMin;
	sum += sec;

	return sum;
    }
}