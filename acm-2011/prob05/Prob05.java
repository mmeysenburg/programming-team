import java.util.*;

public class Prob05 {
    public static void main(String[] args) {
	ValWt[] coins = new ValWt[5];
	coins[0] = new ValWt(1, 2.5);
	coins[1] = new ValWt(5, 5);
	coins[2] = new ValWt(10, 2.268);
	coins[3] =  new ValWt(25, 5.670);
	coins[4] = new ValWt(50, 11.340);

	Scanner stdin = new Scanner(System.in);
	int i = 1;
	int amt = stdin.nextInt();
	while(amt != -1) {
	    int no50s = stdin.nextInt();

	    System.out.println("Request " + i + ": ");
	    i++;
	    amt = stdin.nextInt();
	}
    }
}

    class ValWt {
	int val;
	double mass;

	public ValWt(int val, double mass) {
	    this.val = val;
	    this.mass = mass;
	}
    }
