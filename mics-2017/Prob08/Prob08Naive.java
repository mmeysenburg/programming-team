
import java.util.Scanner;

public class Prob08Naive {
    
    private static long f(long y) {
        if(y <= -3) {
            return y;
        } else if(y < 3) {
            return 2 * y;
        } else {
            long v = f(y - 6) + f(y - 4) + f(y - 1);
            return v;
        }
    }
    
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int n = stdin.nextInt();
        for(int i = 0; i < n; i++) {
            int y = stdin.nextInt();
            System.out.println("Case " + (i + 1) + ": " + f(y));
        }
    }
}