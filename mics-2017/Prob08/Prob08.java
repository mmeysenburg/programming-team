
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Solution to problem 8 from the 2017 MICS programming contest.
 * 
 * @author Mark M. Meysenburg
 * @version 04/08/2017
 */
public class Prob08 {
    
    // map remembers which values we've already computed
    private static Map<Long, Long> map = new HashMap<>();
    
    /**
     * Recursively compute the value of f as described in the problem.
     * 
     * @param n Parameter to the function
     * @return Value of the function
     */
    private static long f(long n) {
        // if we've computed this value already, send the answer back
        if(map.containsKey(n)) {
            return map.get(n);
        }
        
        // otherwise, start recursing
        if(n <= -3) {
            map.put(n, n);
            return n;
        } else if(n < 3) {
            map.put(n, 2 * n);
            return 2 * n;
        } else {
            long v = f(n - 6) + f(n - 4) + f(n - 1);
            map.put(n, v);
            return v;
        }
    }
    
    public static void main(String[] args) {
        
        long start = System.currentTimeMillis();
        
        Scanner stdin = new Scanner(System.in);
        int num = stdin.nextInt();
        for(int i = 0; i < num; i++) {
            int n = stdin.nextInt();
            System.out.println("Case " + (i + 1) + ": " + f(n));
        }
        
        long stop = System.currentTimeMillis();
        
        System.out.println((stop - start));
    }
}