
import java.util.Random;
import java.util.TreeSet;

public class MakeInput {
    public static void main(String[] args) {
        Random prng = new Random();
        System.out.println("10");
        for(int i = 0; i < 10; i++) {
            System.out.print((prng.nextInt(2000) - 1000) + " ");
            TreeSet<Integer> set = new TreeSet<>();
            for(int j = 0; j < 200; j++) {
                set.add(prng.nextInt(2000) - 1000);
            }
            System.out.print(set.size() + " ");
            int[] pos = new int[set.size()];
            int k = 0;
            for(int m : set) {
                pos[k++] = m;
            }
            for(k = 0; k < pos.length; k++) {
                int m = prng.nextInt(pos.length);
                int t = pos[m];
                pos[m] = pos[k];
                pos[k] = t;
            }
            for(k = 0; k < pos.length; k++) {
                System.out.print(pos[k] + " ");
            }
            System.out.println("");
        }
    }
}