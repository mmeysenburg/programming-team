
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Naive solution to problem 1 from MICS 2017 programming contest.
 * 
 * @author Mark M. Meysenburg
 * @version 4/9/2017
 */
public class Prob01 {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // read number of cases, and skip the endline input
        int n = stdin.nextInt();
        stdin.nextLine();
        
        // loop for each case
        for (int i = 1; i <= n; i++) {
            // read line and get Mrs. Plum's position and egg positions
            String line = stdin.nextLine();
            ArrayList<Integer> list = new ArrayList<>();
            Scanner lineIn = new Scanner(line);
            int pos = lineIn.nextInt();
            lineIn.nextInt(); // ignore number of eggs
            while (lineIn.hasNext()) {
                list.add(lineIn.nextInt());
            }

            // order will hold Mrs. Plum's plan
            ArrayList<Integer> order = new ArrayList<>();

            // look at all the remaining egg locations
            while (!list.isEmpty()) {
                // find nearest remaining egg
                int dis = Integer.MAX_VALUE;
                int disIdx = -1;
                int j = 0;
                for (int egg : list) {
                    int d = (pos - egg) * (pos - egg);
                    if (d < dis) {
                        dis = d;
                        disIdx = j;
                    }
                    j++;
                }
                
                // that's where we go next, so save it
                pos = list.get(disIdx);
                order.add(pos);
                list.remove(disIdx);

            }

            // dump the output for the case
            System.out.print("Case " + i + ": ");
            for(int egg : order) {
                System.out.print(egg + " ");
            }
            System.out.println("");
        }
    }
}
